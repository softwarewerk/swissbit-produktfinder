FROM phpstorm/php-71-apache-xdebug

# Make port 80 available to the world outside this container
EXPOSE 80

# !wichtig
ENV APACHE_DOCUMENT_ROOT /var/www/contao/web
# Hier gefunden: https://github.com/docker-library/php/issues/62


RUN apt-get update \
 && apt-get install -y git zlib1g-dev libpng-dev libicu-dev \
 && docker-php-ext-configure intl \
 && docker-php-ext-install pdo pdo_mysql zip gd mbstring intl \
 && a2enmod rewrite \
# && sed -i 's!/var/www/html!/var/www/public!g' /etc/apache2/sites-available/000-default.conf \
 && mv /var/www/html /var/www/public \
 && curl -sS https://getcomposer.org/installer \
  | php -- --install-dir=/usr/local/bin --filename=composer

WORKDIR /var/www/contao/web

RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf
