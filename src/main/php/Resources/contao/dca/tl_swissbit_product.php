<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 09:08 - 09.02.18
 */

$GLOBALS['TL_DCA']['tl_swissbit_product'] = [
    'config' => [
        'dataContainer'    => 'Table',
        'switchToEdit'     => true,
        'enableVersioning' => false,
//        'sql'              => [
//            'keys' => [
//                'id' => 'primary',
//            ]
//        ]
    ],

    'list' => [
        'sorting'           => [
            'mode'         => 1,
            'fields'       => [ 'Description' ],
            'headerFields' => [ 'Description' ],
            'flag'         => 1,
            'panelLayout'  => 'debug;filter;sort,search,limit',
        ],
        'label'             => [
            'fields'      => [ 'Description' ],
            'format'      => '%s',
            'showColumns' => true,
        ],
        'global_operations' => [
        ],
        'operations'        => [
            'edit'       => [
                'label' => &$GLOBALS['TL_LANG']['tl_swissbit_product']['edit'],
                'href'  => 'table=tl_swissbit_product',
                'icon'  => 'edit.gif'
            ],
            'editheader' => [
                'label' => &$GLOBALS['TL_LANG']['tl_swissbit_product']['editheader'],
                'href'  => 'act=edit',
                'icon'  => 'header.gif',
            ],
            'copy'       => [
                'label' => &$GLOBALS['TL_LANG']['tl_swissbit_product']['copy'],
                'href'  => 'act=copy',
                'icon'  => 'copy.gif',
            ],
            'delete'     => [
                'label'      => &$GLOBALS['TL_LANG']['tl_swissbit_product']['delete'],
                'href'       => 'act=delete',
                'icon'       => 'delete.gif',
                'attributes' => 'onclick="if(!confirm(\'' . $GLOBALS['TL_LANG']['MSC']['deleteConfirm'] . '\'))return false;Backend.getScrollOffset()"',
            ],
            'show'       => [
                'label' => &$GLOBALS['TL_LANG']['tl_swissbit_product']['show'],
                'href'  => 'act=show',
                'icon'  => 'show.gif'
            ]
        ]
    ],

    'palettes' => [
        '__selector__' => [],
        'default'      => implode( ',',
            [
                'Description',
                'Status',
                'Type',
                'Series',
                'Family',
                'Density',
                'DensityClass',
                'SwissbitPartNumber',
                'TemperatureGrade',
                'FlashType',
                'ForNewDesign',
                'FactSheetWeblink',
                'ProductPictureWeblink',
                'Endurance',
                'RelCost',
                'SeqReadPerformance',
                'SeqWritePerformance',
                'RandReadPerformance',
                'RandWritePerformance',

                'SmallDataLogging',
                'LargeDataLogging',
                'ImageRecording',
                'VideoRecording',
                'BootAndProgramExec',
                'BootAndVideoPlay',
                'BootAndDatabase',
                'LicenseAndAuthentication'
            ] ),
    ],

    'subpalettes' => [
        '' => ''
    ],

    'fields' => [
        'id'                       => [
            'sql' => "int(10) unsigned NOT NULL auto_increment"
        ],
        'tstamp'                   => [
            'sql' => "int(10) unsigned NOT NULL default '0'"
        ],
        'Status'                   => [
            'label'     => &$GLOBALS['TL_LANG']['tl_swissbit_product']['status'],
            'exclude'   => true,
            'search'    => true,
            'sorting'   => true,
            'flag'      => 1,
            'inputType' => 'select',
            'options'   => array( 'in_production', 'in_development' ),
            'reference' => array( 'in_production' => 'in Production', 'in_development' => 'in Development' ),
            'eval'      => [ 'mandatory' => false, 'maxlength' => 255 ],
//            'sql'       => "varchar(255) NULL"
        ],
        'Type'                     => [
            'label'     => &$GLOBALS['TL_LANG']['tl_swissbit_product']['type'],
            'exclude'   => true,
            'search'    => true,
            'sorting'   => true,
            'flag'      => 1,
            'inputType' => 'text',
            'eval'      => [ 'mandatory' => false, 'maxlength' => 255 ],
//            'sql'       => "varchar(255) NULL"
        ],
        'Series'                   => [
            'label'     => &$GLOBALS['TL_LANG']['tl_swissbit_product']['series'],
            'exclude'   => true,
            'search'    => true,
            'sorting'   => true,
            'flag'      => 1,
            'inputType' => 'text',
            'eval'      => [ 'mandatory' => false, 'maxlength' => 255 ],
//            'sql'       => "varchar(255) NULL"
        ],
        'Family'                   => [
            'label'     => &$GLOBALS['TL_LANG']['tl_swissbit_product']['family'],
            'exclude'   => true,
            'search'    => true,
            'sorting'   => true,
            'flag'      => 1,
            'inputType' => 'text',
            'eval'      => [ 'mandatory' => false, 'maxlength' => 255 ],
//            'sql'       => "varchar(255) NULL"
        ],
        'Density'                  => [
            'label'     => &$GLOBALS['TL_LANG']['tl_swissbit_product']['density'],
            'exclude'   => true,
            'search'    => true,
            'sorting'   => true,
            'flag'      => 1,
            'inputType' => 'text',
            'eval'      => [ 'mandatory' => false, 'maxlength' => 255 ],
//            'sql'       => "varchar(255) NULL"
        ],
        'DensityClass'             => [
            'label'     => &$GLOBALS['TL_LANG']['tl_swissbit_product']['density_class'],
            'exclude'   => true,
            'search'    => true,
            'sorting'   => true,
            'flag'      => 1,
            'inputType' => 'text',
            'eval'      => [ 'mandatory' => false, 'maxlength' => 255 ],
//            'sql'       => "varchar(255) NULL"
        ],
        'SwissbitPartNumber'       => [
            'label'     => &$GLOBALS['TL_LANG']['tl_swissbit_product']['swissbit_part_number'],
            'exclude'   => true,
            'search'    => true,
            'sorting'   => true,
            'flag'      => 1,
            'inputType' => 'text',
            'eval'      => [ 'mandatory' => false, 'maxlength' => 255 ],
//            'sql'       => "varchar(255) NULL"
        ],
        'Description'              => [
            'label'     => &$GLOBALS['TL_LANG']['tl_swissbit_product']['description'],
            'exclude'   => true,
            'search'    => true,
            'sorting'   => true,
            'flag'      => 1,
            'inputType' => 'text',
            'eval'      => [ 'mandatory' => true, 'maxlength' => 255 ],
//            'sql'       => "varchar(255) NULL"
        ],
        'TemperatureGrade'         => [
            'label'     => &$GLOBALS['TL_LANG']['tl_swissbit_product']['temperature_grade'],
            'exclude'   => true,
            'search'    => true,
            'sorting'   => true,
            'flag'      => 1,
            'inputType' => 'text',
            'eval'      => [ 'mandatory' => false, 'maxlength' => 255 ],
//            'sql'       => "varchar(255) NULL"
        ],
        'FlashType'                => [
            'label'     => &$GLOBALS['TL_LANG']['tl_swissbit_product']['flash_type'],
            'exclude'   => true,
            'search'    => true,
            'sorting'   => true,
            'flag'      => 1,
            'inputType' => 'text',
            'eval'      => [ 'mandatory' => false, 'maxlength' => 255 ],
//            'sql'       => "varchar(255) NULL"
        ],
        'ForNewDesign'             => [
            'label'     => &$GLOBALS['TL_LANG']['tl_swissbit_product']['for_new_design'],
            'exclude'   => true,
            'search'    => true,
            'sorting'   => true,
            'flag'      => 1,
            'inputType' => 'select',
            'options'   => array( 'no', 'yes' ),
            'reference' => array( 'no' => 'No', 'yes' => 'Yes' ),
//            'sql'       => "varchar(10) NULL"
        ],
        'FactSheetWeblink'         => [
            'label'     => &$GLOBALS['TL_LANG']['tl_swissbit_product']['fact_sheet_weblink'],
            'exclude'   => true,
            'search'    => true,
            'sorting'   => true,
            'flag'      => 1,
            'inputType' => 'text',
            'eval'      => [ 'mandatory' => false, 'maxlength' => 255 ],
//            'sql'       => "varchar(255) NULL"
        ],
        'ProductPictureWeblink'    => [
            'label'     => &$GLOBALS['TL_LANG']['tl_swissbit_product']['product_picture_weblink'],
            'exclude'   => true,
            'search'    => true,
            'sorting'   => true,
            'flag'      => 1,
            'inputType' => 'text',
            'eval'      => [ 'mandatory' => false, 'maxlength' => 255 ],
//            'sql'       => "varchar(255) NULL"
        ],
        'Endurance'                => [
            'label'     => &$GLOBALS['TL_LANG']['tl_swissbit_product']['product_picture_weblink'],
            'exclude'   => true,
            'search'    => true,
            'sorting'   => true,
            'flag'      => 1,
            'inputType' => 'select',
            'options'   => array( '+', '++', '+++', '++++' ),
            'eval'      => [ 'mandatory' => false, 'maxlength' => 10 ],
//            'sql'       => "varchar(10) NULL"
        ],
        'RelCost'                  => [
            'label'     => &$GLOBALS['TL_LANG']['tl_swissbit_product']['rel_cost'],
            'exclude'   => true,
            'search'    => true,
            'sorting'   => true,
            'flag'      => 1,
            'inputType' => 'select',
            'options'   => array( '$', '$$', '$$$', '$$$$' ),
            'eval'      => [ 'mandatory' => false, 'maxlength' => 10 ],
//            'sql'       => "varchar(10) NULL"
        ],
        'SeqReadPerformance'       => [
            'label'       => &$GLOBALS['TL_LANG']['tl_swissbit_product']['seq_read_performance'],
            'explanation' => 'Sequential read performance in MB/s',
            'exclude'     => true,
            'search'      => true,
            'sorting'     => true,
            'flag'        => 1,
            'inputType'   => 'text',
            'eval'        => [ 'mandatory' => false, 'maxlength' => 10 ],
//            'sql'         => "varchar(10) NULL"
        ],
        'SeqWritePerformance'      => [
            'label'       => &$GLOBALS['TL_LANG']['tl_swissbit_product']['seq_write_performance'],
            'explanation' => 'Sequential write performance in MB/s',
            'exclude'     => true,
            'search'      => true,
            'sorting'     => true,
            'flag'        => 1,
            'inputType'   => 'text',
            'eval'        => [ 'mandatory' => false, 'maxlength' => 10 ],
            'sql'         => "varchar(10) NULL"
        ],
        'RandReadPerformance'      => [
            'label'       => &$GLOBALS['TL_LANG']['tl_swissbit_product']['rand_read_performance'],
            'explanation' => 'Random read performance IOPS',
            'exclude'     => true,
            'search'      => true,
            'sorting'     => true,
            'flag'        => 1,
            'inputType'   => 'text',
            'eval'        => [ 'mandatory' => false, 'maxlength' => 10 ],
//            'sql'         => "varchar(10) NULL"
        ],
        'RandWritePerformance'     => [
            'label'     => &$GLOBALS['TL_LANG']['tl_swissbit_product']['rand_write_performance'],
            'exclude'   => true,
            'search'    => true,
            'sorting'   => true,
            'flag'      => 1,
            'inputType' => 'text',
            'eval'      => [ 'mandatory' => false, 'maxlength' => 10 ],
//            'sql'       => "varchar(10) NULL"
        ],


        'SmallDataLogging'         => [
            'label'     => &$GLOBALS['TL_LANG']['tl_swissbit_product']['small_data_logging'],
            'exclude'   => true,
            'search'    => true,
            'sorting'   => true,
            'flag'      => 1,
            'inputType' => 'text',
            'eval'      => [ 'mandatory' => false, 'maxlength' => 10 ],
//            'sql'       => "int(10) unsigned NULL"
        ],
        'LargeDataLogging'         => [
            'label'     => &$GLOBALS['TL_LANG']['tl_swissbit_product']['large_data_logging'],
            'exclude'   => true,
            'search'    => true,
            'sorting'   => true,
            'flag'      => 1,
            'inputType' => 'text',
            'eval'      => [ 'mandatory' => false, 'maxlength' => 10 ],
            'sql'       => "int(10) unsigned NULL"
        ],
        'ImageRecording'           => [
            'label'     => &$GLOBALS['TL_LANG']['tl_swissbit_product']['image_recording'],
            'exclude'   => true,
            'search'    => true,
            'sorting'   => true,
            'flag'      => 1,
            'inputType' => 'select',
            'options'   => array( 0, 1 ),
            'reference' => array( 0 => 'No', '1' => 'Yes' ),
            'eval'      => [ 'mandatory' => false, 'maxlength' => 10 ],
//            'sql'       => "tinyint NULL"
        ],
        'VideoRecording'           => [
            'label'     => &$GLOBALS['TL_LANG']['tl_swissbit_product']['video_recording'],
            'exclude'   => true,
            'search'    => true,
            'sorting'   => true,
            'flag'      => 1,
            'inputType' => 'select',
            'options'   => array( 0, 1 ),
            'reference' => array( 0 => 'No', '1' => 'Yes' ),
            'eval'      => [ 'mandatory' => false, 'maxlength' => 10 ],
//            'sql'       => "tinyint NULL"
        ],
        'BootAndProgramExec'       => [
            'label'     => &$GLOBALS['TL_LANG']['tl_swissbit_product']['boot_and_program_exec'],
            'exclude'   => true,
            'search'    => true,
            'sorting'   => true,
            'flag'      => 1,
            'inputType' => 'select',
            'options'   => array( 0, 1 ),
            'reference' => array( 0 => 'No', '1' => 'Yes' ),
            'eval'      => [ 'mandatory' => false, 'maxlength' => 10 ],
//            'sql'       => "tinyint NULL"
        ],
        'BootAndVideoPlay'         => [
            'label'     => &$GLOBALS['TL_LANG']['tl_swissbit_product']['boot_and_video_play'],
            'exclude'   => true,
            'search'    => true,
            'sorting'   => true,
            'flag'      => 1,
            'inputType' => 'select',
            'options'   => array( 0, 1 ),
            'reference' => array( 0 => 'No', '1' => 'Yes' ),
            'eval'      => [ 'mandatory' => false, 'maxlength' => 10 ],
//            'sql'       => "tinyint NULL"
        ],
        'BootAndDatabase'          => [
            'label'     => &$GLOBALS['TL_LANG']['tl_swissbit_product']['boot_and_database'],
            'exclude'   => true,
            'search'    => true,
            'sorting'   => true,
            'flag'      => 1,
            'inputType' => 'select',
            'options'   => array( 0, 1 ),
            'reference' => array( 0 => 'No', '1' => 'Yes' ),
            'eval'      => [ 'mandatory' => false, 'maxlength' => 10 ],
//            'sql'       => "tinyint NULL"
        ],
        'LicenseAndAuthentication' => [
            'label'     => &$GLOBALS['TL_LANG']['tl_swissbit_product']['license_and_authentication'],
            'exclude'   => true,
            'search'    => true,
            'sorting'   => true,
            'flag'      => 1,
            'inputType' => 'select',
            'options'   => array( 0, 1 ),
            'reference' => array( 0 => 'No', '1' => 'Yes' ),
            'eval'      => [ 'mandatory' => false, 'maxlength' => 10 ],
//            'sql'       => "tinyint NULL"
        ],
    ]
];

