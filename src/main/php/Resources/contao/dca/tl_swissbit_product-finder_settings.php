<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 09:08 - 09.02.18
 */

array_insert( $GLOBALS['TL_DCA']['tl_swissbit_product-finder_settings'], 0, [
    'config' => [
        'dataContainer' => 'File',
        'closed'        => true,
    ],

    'palettes' => [
        '__selector__' => [],
        'default'      => implode( ';', [
            implode( ',',
                [
                    '{mail_settings_legend}',
                    'swissbit_product-finder_mail_sender_email',
                    'swissbit_product-finder_mail_sender_name',
                    'swissbit_product-finder_mail_team'
                ] ),
            implode( ',',
                [
                    '{octopart_settings_legend}',
                    'swissbit_product-finder_octopart_api_base_url',
                    'swissbit_product-finder_octopart_api_key'
                ] ),
            implode( ',',
                [
                    '{product-list_settings_legend}',
                    'swissbit_product-finder_product-list_page-size'
                ] )
        ] ),
    ],

    'subpalettes' => [
        '' => ''
    ],

    'fields' => [
        'swissbit_product-finder_mail_sender_email'      => [
            'label'          => &$GLOBALS['TL_LANG']['tl_swissbit_product-finder_settings']['mail_sender_email'],
            'inputType'      => 'text',
            'decodeEntities' => false,
            'eval'           => array( 'mandatory' => true, 'preserveTags' => true, 'tl_class' => 'w50' )
        ],
        'swissbit_product-finder_mail_sender_name'       => [
            'label'          => &$GLOBALS['TL_LANG']['tl_swissbit_product-finder_settings']['mail_sender_name'],
            'inputType'      => 'text',
            'decodeEntities' => false,
            'eval'           => array( 'mandatory' => true, 'preserveTags' => true, 'tl_class' => 'w50' )
        ],
        'swissbit_product-finder_mail_team'              => [
            'label'          => &$GLOBALS['TL_LANG']['tl_swissbit_product-finder_settings']['mail_team'],
            'inputType'      => 'textarea',
            'decodeEntities' => false,
            'eval'           => array( 'mandatory' => true, 'preserveTags' => true, 'tl_class' => 'w50' )
        ],
        'swissbit_product-finder_octopart_api_base_url'  => [
            'label'     => &$GLOBALS['TL_LANG']['tl_swissbit_product-finder_settings']['octopart_api_base_url'],
            'inputType' => 'text',
            'eval'      => array( 'mandatory' => true, 'tl_class' => 'w50' )
        ],
        'swissbit_product-finder_octopart_api_key'       => [
            'label'     => &$GLOBALS['TL_LANG']['tl_swissbit_product-finder_settings']['octopart_api_key'],
            'inputType' => 'text',
            'eval'      => array( 'mandatory' => true, 'tl_class' => 'w50' )
        ],
        'swissbit_product-finder_product-list_page-size' => [
            'label'     => &$GLOBALS['TL_LANG']['tl_swissbit_product-finder_settings']['product-list_page-size'],
            'inputType' => 'text',
            'eval'      => array( 'mandatory' => true, 'rgxp' => 'natural', 'tl_class' => 'w50' )
        ]
    ]
] );
