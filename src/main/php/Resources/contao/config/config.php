<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 15:42 - 30.01.18
 */

/**
 * Content elements
 */
array_insert( $GLOBALS['TL_CTE']['includes'], 1, [
    'dummy'             => \netshake\SwissbitProductFinder\Contao\ContentElement\DummyContentElement::class,
    'swissbit-use-case' => \netshake\SwissbitProductFinder\Contao\ContentElement\UseCaseContentElement::class,
] );

/**
 * Front end modules
 */
array_insert( $GLOBALS['FE_MOD']['swissbit-product-finder'], 1, [
    'swissbit-product-finder-filter'       => \netshake\SwissbitProductFinder\Contao\Module\FilterModule::class,
    'swissbit-product-finder-use-case'     => \netshake\SwissbitProductFinder\Contao\Module\UseCaseModule::class,
    'swissbit-product-finder-product-list' => \netshake\SwissbitProductFinder\Contao\Module\ProductListModule::class,
] );

/**
 * Back end modules
 */
array_insert( $GLOBALS['BE_MOD']['swissbit-product-finder'], 1, [
    'Products' => [
        'tables' => [ 'tl_swissbit_product' ],
        // 'icon' => 'bundles/xuadcar/icon.png',
        'table'  => [ 'TableWizard', 'importTable' ],
        'list'   => [ 'ListWizard', 'importList' ]
    ],
    'Settings' => [
        'tables' => [ 'tl_swissbit_product-finder_settings' ],
        // 'icon' => 'bundles/xuadcar/icon.png',
//        'table'  => [ 'TableWizard', 'importTable' ],
//        'list'   => [ 'ListWizard', 'importList' ]
    ]
] );
