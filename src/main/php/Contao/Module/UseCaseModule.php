<?php

namespace netshake\SwissbitProductFinder\Contao\Module;

use Contao\Module;
use netshake\SwissbitProductFinder\Di\Service\ProductFilterService;

/**
 * Class UseCaseModule
 *
 * @package netshake\SwissbitProductFinder\Contao\Module
 */
class UseCaseModule extends AbstractModule
{
    /**
     * @var string
     */
    protected $strTemplate = 'mod_use_case';

    /**
     * Generate the module
     */
    protected function _compile()
    {
//        /** @var \Symfony\Bundle\FrameworkBundle\Routing\Router $router */
//        $router = \System::getContainer()->get('router');
//        var_dump( $router->generate( 'swissbit_product_finder_index_index' ) );

//        /** @var ProductFilterService $productFilter */
//        $productFilter = \System::getContainer()->get( ProductFilterService::class );
//
//
//        $this->Template->callBackUrl      = $this->Environment->requestUri;
//        $this->Template->useCases         = array(
//            'use-case-1',
//            'use-case-2',
//            'use-case-3',
//            'use-case-4',
//            'use-case-5',
//            'use-case-6',
//            'use-case-7'
//        );
//        $this->Template->selectedUseCases = array(); // $productFilter->getSelectedUseCases();
//
//        $this->Template->html = ( TL_MODE == 'FE' ) ? $this->html : htmlspecialchars( $this->html );
    }
}
