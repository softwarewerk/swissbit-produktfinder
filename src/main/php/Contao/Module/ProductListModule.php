<?php

namespace netshake\SwissbitProductFinder\Contao\Module;

/**
 * Class ProductListModule
 *
 * @package netshake\SwissbitProductFinder\Contao\Module
 */
class ProductListModule extends AbstractModule
{
    /**
     * @var string
     */
    protected $strTemplate = 'mod_product_list';

    /**
     * Generate the module
     */
    protected function _compile()
    {
        $this->Template->pageSize = intval( \Contao\Config::get( 'swissbit_product-finder_product-list_page-size' ) );

        $this->Template->html   = ( TL_MODE == 'FE' ) ? $this->html : htmlspecialchars( $this->html );
        $this->Template->router = \System::getContainer()->get( 'router' );
    }
}
