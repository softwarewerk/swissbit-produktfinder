<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 13:41 - 28.03.18
 */

namespace netshake\SwissbitProductFinder\Contao\Module;

use Contao\Module;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class AbstractModule
 *
 * @package netshake\SwissbitProductFinder\Contao\Module
 */
abstract class AbstractModule extends Module
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @return TranslatorInterface
     */
    protected function getTranslator()
    {
        if( !isset( $this->translator ) ) {
            $this->translator = \System::getContainer()->get( 'translator' );
        }

        return $this->translator;
    }

    /**
     * @return void
     */
    final public function compile()
    {
        // TODO: Abstrahieren
        $this->Template->_ = function ( $key, $params = null, $domain = null ) use ( $translator ) {
            return $this->getTranslator()->trans( $key, (array) $params, $domain );
        };

        $this->_compile();
    }

    /**
     * @return void
     */
    abstract protected function _compile();
}
