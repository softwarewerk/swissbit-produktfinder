<?php

namespace netshake\SwissbitProductFinder\Contao\Module;

use netshake\SwissbitProductFinder\Di\Service\ProductFilterService;
use netshake\SwissbitProductFinder\ProductFilter\Html\FilterRenderer;

/**
 * Class FilterModule
 *
 * @package netshake\SwissbitProductFinder\Contao\Module
 */
class FilterModule extends AbstractModule
{
    /**
     * @var string
     */
    protected $strTemplate = 'mod_filter';

    /**
     * Generate the module
     */
    protected function _compile()
    {
        /** @var ProductFilterService $productFilterService */
        $productFilterService = \System::getContainer()->get( ProductFilterService::class );

        $productFilterService->restoreFiltersFromSession();

        $filterRenderer = new FilterRenderer();
        $filterRenderer->setTranslator( $this->getTranslator() );

        $this->Template->filters        = $productFilterService->getFilters();
        $this->Template->filterRenderer = $filterRenderer;

        $this->Template->html = ( TL_MODE == 'FE' ) ? $this->html : htmlspecialchars( $this->html );
    }
}
