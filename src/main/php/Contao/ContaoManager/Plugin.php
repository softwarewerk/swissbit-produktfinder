<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 14:39 - 30.01.18
 */

namespace netshake\SwissbitProductFinder\Contao\ContaoManager;

use Contao\CoreBundle\ContaoCoreBundle;
use Contao\ManagerPlugin\Bundle\BundlePluginInterface;
use Contao\ManagerPlugin\Bundle\Config\BundleConfig;
use Contao\ManagerPlugin\Bundle\Parser\ParserInterface;
use Contao\ManagerPlugin\Routing\RoutingPluginInterface;
use netshake\SwissbitProductFinder\SwissbitProductFinderBundle;
use Symfony\Component\Config\Loader\LoaderResolverInterface;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Plugin for the Contao Manager.
 */
class Plugin implements BundlePluginInterface, RoutingPluginInterface
{
    /**
     * {@inheritdoc}
     */
    public function getBundles( ParserInterface $parser )
    {
        return [
            BundleConfig::create( SwissbitProductFinderBundle::class )
                        ->setLoadAfter( [ ContaoCoreBundle::class ] )
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getRouteCollection( LoaderResolverInterface $resolver, KernelInterface $kernel )
    {
        return $resolver
            ->resolve( __DIR__ . '/../../Resources/config/routing.yml' )
            ->load( __DIR__ . '/../../Resources/config/routing.yml' );
    }
}
