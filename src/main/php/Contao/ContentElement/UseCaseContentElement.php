<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 15:41 - 30.01.18
 */

namespace netshake\SwissbitProductFinder\Contao\ContentElement;

use Contao\ContentElement;
use Contao\PageModel;
use netshake\SwissbitProductFinder\Di\Service\ProductFilterService;

/**
 * Class UseCaseListContentElement
 *
 * @package netshake\SwissbitProductFinder\Contao\ContentElement
 * @property \Contao\Environment $Environment
 */
class UseCaseContentElement extends ContentElement
{
    protected $strTemplate = 'ce_swissbit_use_case';

    /**
     * Compile the content element
     */
    protected function compile()
    {
        if( TL_MODE == 'FE' ) {
            /** @var PageModel $objPage */
            global $objPage;

            /** @var PageModel $productFinderPage */
            $productFinderPage = PageModel::findOneByPid( $objPage->id );
            $callBackUrl       = $productFinderPage->getAbsoluteUrl();
        } else {
            $callBackUrl = null;
        }

//        /** @var \Symfony\Bundle\FrameworkBundle\Routing\Router $router */
//        $router = \System::getContainer()->get('router');
//        var_dump( $router->generate( 'swissbit_product_finder_index_index' ) );

        /** @var ProductFilterService $productFilter */
        $productFilter = \System::getContainer()->get( ProductFilterService::class );

        $this->Template->callBackUrl      = $callBackUrl; // $this->Environment->requestUri;
        $this->Template->useCases         = array_map( function ( $useCase ) {
            $useCase['img'] = ( $this->Environment->base . $useCase['img'] );

            return $useCase;
        }, ProductFilterService::USE_CASE_SPECS );
        $this->Template->selectedUseCases = (array) $productFilter->getUseCase();

    }
}
