<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 15:41 - 30.01.18
 */

namespace netshake\SwissbitProductFinder\Contao\ContentElement;

use Contao\ContentElement;

/**
 * Class DummyContentElement
 *
 * @package netshake\SwissbitProductFinder\Contao\ContentElement
 */
class DummyContentElement extends ContentElement
{
    protected $strTemplate = 'ce_dummy';

    /**
     * Compile the content element
     */
    protected function compile()
    {
        parent::compile();
    }
}
