<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 19:26 - 09.02.18
 */

namespace netshake\SwissbitProductFinder\Form;

use Zend_View_Interface;


/**
 * Class ProductFilterForm
 *
 * @package netshake\SwissbitProductFinder\Form
 */
class ProductFilterForm extends \Zend_Form
{
    /**
     * @throws \Zend_Form_Exception
     * @return void
     */
    public function init()
    {
        $this->setDisableLoadDefaultDecorators( true );

        $this->addElement( 'select', 'Type', array(
            'disableLoadDefaultDecorators' => true,
            'class'                        => 'form-control',
            'label'                        => 'Type',
            'multiOptions'                 => $this->getFilterFieldValues( 'type' )
        ) );
        $this->addElement( 'select', 'Status', array(
            'disableLoadDefaultDecorators' => true,
            'class'                        => 'form-control',
            'label'                        => 'Status',
            'multiOptions'                 => $this->getFilterFieldValues( 'status' )
        ) );
    }

    /**
     * @return \Zend_Form
     */
    public function loadDefaultDecorators()
    {
        $this
            ->setDecorators( array(
                'FormElements',
                array( array( 'Row' => 'HtmlTag' ), array( 'tag' => 'div', 'class' => 'row' ) ),
                'Form'
            ) )
            ->setElementDecorators( array(
                'Label',
                'ViewHelper',
                array( array( 'FormGroup' => 'HtmlTag' ), array( 'tag' => 'div', 'class' => 'form-group' ) ),
                array( array( 'Col' => 'HtmlTag' ), array( 'tag' => 'div', 'class' => 'col-md-2' ) )
            ) );

        return $this;
    }

    /**
     * @var array
     */
    private $filterValues;

    public function setFilterValues( $filterValues )
    {
        $this->filterValues = $filterValues;

        return $this;
    }

    /**
     * @return array
     */
    public function getFilterFieldValues( $fieldName )
    {
        if( isset( $this->filterValues[$fieldName] ) ) {
            return array_combine( $this->filterValues[$fieldName], $this->filterValues[$fieldName] );
        }

        return array();
    }

    /**
     * @param Zend_View_Interface $view
     *
     * @return string
     */
    public function render( Zend_View_Interface $view = null )
    {
        if( is_null( $view ) && ! isset( $this->_view ) ) {
            $this->setView( new \Zend_View() );
        }

        return parent::render( $view );
    }
}
