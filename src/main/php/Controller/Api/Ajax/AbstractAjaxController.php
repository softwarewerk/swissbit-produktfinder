<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 18:28 - 23.03.18
 */

namespace netshake\SwissbitProductFinder\Controller\Api\Ajax;

use netshake\SwissbitProductFinder\Controller\AbstractController;

/**
 * Class AbstractAjaxController
 *
 * @package netshake\SwissbitProductFinder\Controller\Api\Ajax
 */
abstract class AbstractAjaxController extends AbstractController
{

}
