<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 16:44 - 28.02.18
 */

namespace netshake\SwissbitProductFinder\Controller\Api\Ajax;

use netshake\SwissbitProductFinder\Di\Service\ProductFilterService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ProductFilterController
 *
 * @package netshake\SwissbitProductFinder\Controller\Api\Ajax
 */
class ProductFilterController extends AbstractAjaxController
{
    /**
     * @Route("/product-finder/api/product-filter/",
     *     name="@SwissbitProductFinder:Api:Ajax:ProductFilter")
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function indexAction( Request $request )
    {
        /** @var ProductFilterService $productFilterService */
        $productFilterService = \System::getContainer()->get( ProductFilterService::class );

        $productFilterService->restoreFiltersFromSession()->initFilters( $request->request->all() );
        $productFilterService->rememberFilterData();

        return $this->json( [
            'succes'         => true,
            'active_filters' => $productFilterService->getActiveFilterNames()
        ] );
    }
}
