<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 16:44 - 28.02.18
 */

namespace netshake\SwissbitProductFinder\Controller\Api\Ajax;

use netshake\SwissbitProductFinder\Di\Service\OctopartService;
use netshake\SwissbitProductFinder\Di\Service\ProductService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ProductAvailabilityController
 *
 * @package netshake\SwissbitProductFinder\Controller\Api\Ajax
 */
class ProductAvailabilityController extends AbstractAjaxController
{
    /**
     * @Route("/product-finder/api/availability/rating/",
     *     name="@SwissbitProductFinder:Api:Ajax:ProductAvailability[rating]")
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function ratingAction( Request $request )
    {
        /** @var OctopartService $octopartService */
        $octopartService = $this->container->get( OctopartService::class );

        return $this->json( $octopartService
            ->setProductPartNumbers( $request->get( 'mpn', [] ) )
            ->query()
            ->getItemsAvailabilityRating() );
    }
}
