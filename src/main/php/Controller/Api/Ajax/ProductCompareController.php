<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 15:10 - 25.03.18
 */

namespace netshake\SwissbitProductFinder\Controller\Api\Ajax;

use netshake\SwissbitProductFinder\Di\Service\ProductCompareService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ProductCompareController
 *
 * @package netshake\SwissbitProductFinder\Controller\Api\Ajax
 */
class ProductCompareController extends AbstractAjaxController
{
    /**
     * @Route("/product-finder/api/product-compare/toggle-product/",
     *     name="@SwissbitProductFinder:Api:Ajax:ProductCompare[toggleProduct]")
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function toggleProductAction( Request $request )
    {
        $productId = $request->get( 'id' );
        $selected  = false;

        /** @var ProductCompareService $productCompareService */
        $productCompareService = $this->container->get( ProductCompareService::class );

        $addedProductIds = $productCompareService->restoreFromSession()->getAllProductIds();

        // Already added => remove
        if( in_array( $productId, $addedProductIds ) ) {
            $productCompareService->removeFromCompare( $productId );
        } else if( 5 <= count( $addedProductIds ) ) {
            return $this->json( [
                'error'         => true,
                'error_message' => 'You already have selected 5 products.',
                'selected_product_ids' => $productCompareService->getAllProductIds()
            ] );
        } else {
            $productCompareService->addProductsToCompare( [ $productId ] );
            $selected = true;
        }

        $productCompareService->rememberProducts();

        return $this->json( [
            'selected_product_ids' => $productCompareService->getAllProductIds(),
            'error'    => false,
            'selected' => $selected
        ] );
    }
}
