<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 16:44 - 28.02.18
 */

namespace netshake\SwissbitProductFinder\Controller\Api\Ajax;

use netshake\SwissbitProductFinder\Di\Service\ProductCompareService;
use netshake\SwissbitProductFinder\Di\Service\ProductFilterService;
use netshake\SwissbitProductFinder\Di\Service\ProductService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ProductListController
 *
 * @package netshake\SwissbitProductFinder\Controller\Api\Ajax
 */
class ProductListController extends AbstractAjaxController
{
    /**
     * @Route("/product-finder/api/product-list/html/",
     *     name="@SwissbitProductFinder:Api:Ajax:ProductList[html]")
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function htmlAction( Request $request )
    {
        $pageSize = $request->get( 'n', 0 );
        $offset   = intval( $request->get( 'o', 0 ) );

        /** @var ProductService $productService */
        $productService = \System::getContainer()->get( ProductService::class );

        /** @var ProductCompareService $productCompareService */
        $productCompareService = $this->container->get( ProductCompareService::class );

        /** @var ProductFilterService $productFilterService */
        $productFilterService = $this->container->get( ProductFilterService::class );

        $productFilterService->restoreFiltersFromSession();
        $productCompareService->restoreFromSession();

        // Not all items are visible -> so we clear the product compare.
        if( 0 == $offset ) {
            // We clear the selected items
            $productCompareService->clear()->rememberProducts();
        }

        $items = $productService->findAllUsingFilter( $offset, $pageSize );

        $twigParams = [
            'use_case_key'   => $productFilterService->getUseCaseEntityPropertyName(),
            'items'          => $items,
            'selected_items' => $productCompareService->getAllProductIds()
        ];

        return $this->json( [
            'items' => count( $items ),
            'html'  => [
                'table_format' => $this->renderView(
                    '@SwissbitProductFinder/api/ajax/product-list/format-table.twig',
                    $twigParams
                ),
                'grid_format'  => $this->renderView(
                    '@SwissbitProductFinder/api/ajax/product-list/format-grid.twig',
                    $twigParams
                )
            ]
        ] );
    }

    /**
     * @Route("/product-finder/api/product-list/reset/",
     *     name="@SwissbitProductFinder:Api:Ajax:ProductList[reset]")
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function resetAction( Request $request )
    {
        /** @var ProductCompareService $productCompareService */
        $productCompareService = $this->container->get( ProductCompareService::class );

        // We clear the selected items
        $productCompareService->clear()->rememberProducts();

        return $this->json( [
            'success' => true,
        ] );
    }
}
