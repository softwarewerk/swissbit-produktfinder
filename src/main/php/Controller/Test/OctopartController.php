<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 15:38 - 08.02.18
 */

namespace netshake\SwissbitProductFinder\Controller\Test;

use netshake\SwissbitProductFinder\Controller\AbstractController;
use netshake\SwissbitProductFinder\Di\Service\OctopartService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class OctopartController
 *
 * @package netshake\SwissbitProductFinder\Controller\Test
 */
class OctopartController extends AbstractController
{
    /**
     * @Route("/product-finder/test/octopart/", name="@SwissbitProductFinder:Test:Octopart")
     * @param Request $request
     *
     * @return void
     */
    public function indexAction( Request $request )
    {
        $this->container->get( 'contao.framework' )->initialize();

        /** @var OctopartService $octopartService */
        $octopartService = $this->container->get( OctopartService::class );

        $octopartService->addProductPartNumber( 'SFCF1024H1BK2TO-I-MS-553-SMA' )
                        ->addProductPartNumber( 'SFCF4096H1BK2MT-C-QP-553-SMA' )
                        ->query();

        echo "<pre>";
        var_export( $octopartService->getItemOffers() );
        echo "</pre>";

        die( 'asd' );
    }
}
