<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 12:14 - 29.03.18
 */

namespace netshake\SwissbitProductFinder\Controller\Test;

use netshake\SwissbitProductFinder\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class MailController
 *
 * @package netshake\SwissbitProductFinder\Controller\Test
 */
class MailController extends AbstractController
{
    /**
     * @Route("/product-finder/test/mail/", name="@SwissbitProductFinder:Test:Mail")
     * @param Request $request
     *
     * @return void
     */
    public function indexAction( Request $request )
    {
        $mail = $this->newEmail( 'Test-Email' );
        $mail->text = <<<text
Hallo,

Dies ist eine Test-Email. 
text;

        $this->sendEmailToTeam( $mail );

        die( 'asd' );
    }
}
