<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 07:55 - 22.03.18
 */

namespace netshake\SwissbitProductFinder\Controller\Test;


use netshake\SwissbitProductFinder\Controller\AbstractController;
use netshake\SwissbitProductFinder\Di\Service\ProductCompareService;
use netshake\SwissbitProductFinder\Di\Service\ProductService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class OctopartController
 *
 * @package netshake\SwissbitProductFinder\Controller\Test
 */
class ProductCompareControlle extends AbstractController
{
    /**
     * @Route("/product-finder/test/product-compare/", name="@SwissbitProductFinder:Test:ProductCompare")
     * @param Request $request
     *
     * @return void
     */
    public function indexAction( Request $request )
    {
        $this->container->get( 'contao.framework' )->initialize();

        /** @var ProductService $productService */
        $productService = $this->container->get( ProductService::class );

        /** @var ProductCompareService $productCompareService */
        $productCompareService = $this->container->get( ProductCompareService::class );

        $additionalProduct = $productService->find( 323 );
        $additionalProduct2 = $productService->find( 211 );

//        $productCompareService->setProducts( $productService->findRandomProducts( 5 ) )->rememberProducts();

        $productCompareService->restoreFromSession();

//        $productCompareService->addToCompare( $additionalProduct )->rememberProducts();

//        $productCompareService->removeFromCompare( $additionalProduct )->rememberProducts();

//        $productCompareService->addToCompare( $additionalProduct2 );


        var_dump( $productCompareService->getAllProductIds() );

//        $productCompareService->addProductsToCompare( array(
//
//        ) );

//        var_dump(  );

//        echo "<pre>";
//        var_export( $octopartService->getItemOffers() );
//        echo "</pre>";

        die( 'added-products-to-comapre' );
    }

    /**
     * @Route("/product-finder/test/product-compare/compare.pdf",
     *     name="@SwissbitProductFinder:Test:ProductCompare[export-pdf]")
     * @param Request $request
     *
     * @return void
     */
    public function exportPdfAction( Request $request )
    {
        die( 'make-pdf' );
    }

    /**
     * @Route("/product-finder/test/product-compare/compare.xlsx",
     *     name="@SwissbitProductFinder:Test:ProductCompare[export-xslx]")
     * @param Request $request
     *
     * @return void
     */
    public function exportXlsxAction( Request $request )
    {
        die( 'make-xlsx' );
    }
}
