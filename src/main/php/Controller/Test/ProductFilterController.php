<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 15:38 - 08.02.18
 */

namespace netshake\SwissbitProductFinder\Controller\Test;

use netshake\SwissbitProductFinder\Controller\AbstractController;
use netshake\SwissbitProductFinder\Di\Service\ProductFilterService;
use netshake\SwissbitProductFinder\Di\Service\ProductService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ProductFilterController
 *
 * @package netshake\SwissbitProductFinder\Controller\Test
 */
class ProductFilterController extends AbstractController
{
    /**
     * @Route("/product-finder/test/product-filter/", name="@SwissbitProductFinder:Test:ProductFilter")
     * @param Request $request
     *
     * @return void
     */
    public function indexAction( Request $request )
    {
        $this->container->get( 'contao.framework' )->initialize();

        /** @var ProductService $productService */
        $productService = $this->container->get( ProductService::class );

        /** @var ProductFilterService $productFilterService */
        $productFilterService = $this->container->get( ProductFilterService::class );

        $productFilterService->restoreFiltersFromSession();

        echo "<pre>", $productFilterService->explain(), "</pre>";

//        $productImportService->setSourceFile( '/home/web/www/swissbit/var/swissbit/swissbit_product_database.xlsx');
//        $productImportService->import();

        die( 'asd' );
    }

    /**
     * @Route("/product-finder/test/product-filter/store/", name="@SwissbitProductFinder:Test:ProductFilter[store]")
     * @param Request $request
     *
     * @return void
     */
    public function storeAction( Request $request )
    {
        $this->container->get( 'contao.framework' )->initialize();

        /** @var ProductFilterService $productFilterService */
        $productFilterService = $this->container->get( ProductFilterService::class );

        $productFilterService->restoreFiltersFromSession();

        $productFilterService->storeFilterValues();

        $productFilterService->restoreFilterValues( $productFilterService->getFilterId() );

        var_dump( $productFilterService->getFilterId() );

        echo "<pre>", $productFilterService->explain(), "</pre>";

//        $productImportService->setSourceFile( '/home/web/www/swissbit/var/swissbit/swissbit_product_database.xlsx');
//        $productImportService->import();

        die( 'asd' );
    }

    /**
     * @Route("/product-finder/test/product-filter/product-list/", name="@SwissbitProductFinder:Test:ProductFilter[product-list]")
     * @param Request $request
     *
     * @return void
     */
    public function productListAction( Request $request )
    {
        $this->container->get( 'contao.framework' )->initialize();

//        /** @var ProductFilterService $productFilterService */
//        $productFilterService = $this->container->get( ProductFilterService::class );

        /** @var ProductFilterService $productFilter */
        $productFilter = $this->container->get( ProductFilterService::class );

        /** @var ProductService $productService */
        $productService = $this->container->get( ProductService::class );

        $productFilter->restoreFiltersFromSession();

        var_dump( $productService->findAllUsingFilter() );

//        $productImportService->setSourceFile( '/home/web/www/swissbit/var/swissbit/swissbit_product_database.xlsx');
//        $productImportService->import();

        die( 'asd' );
    }
}
