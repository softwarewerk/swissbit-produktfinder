<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 15:38 - 08.02.18
 */

namespace netshake\SwissbitProductFinder\Controller\Test;

use netshake\SwissbitProductFinder\Controller\AbstractController;
use netshake\SwissbitProductFinder\Di\Service\ProductService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ProductController
 *
 * @package netshake\SwissbitProductFinder\Controller\Test
 */
class ProductController extends AbstractController
{
    /**
     * @Route("/product-finder/test/product/", name="@SwissbitProductFinder:Test:Product[product]")
     * @param Request $request
     *
     * @return void
     */
    public function productAction( Request $request )
    {
//        $this->container->get( 'contao.framework' )->initialize();

        /** @var ProductService $productService */
        $productService = $this->container->get( ProductService::class );

//        var_dump(  $productService->getClassMetadata()->getIdentifier() );

        var_dump( $productService->findAll() );

//        $productImportService->setSourceFile( '/home/web/www/swissbit/var/swissbit/swissbit_product_database.xlsx');
//        $productImportService->import();

        die( 'asd' );
    }
}
