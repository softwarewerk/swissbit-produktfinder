<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 15:38 - 08.02.18
 */

namespace netshake\SwissbitProductFinder\Controller\Test;

use netshake\SwissbitProductFinder\Controller\AbstractController;
use netshake\SwissbitProductFinder\Di\Service\ProductImportService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ProductImportController
 *
 * @package netshake\SwissbitProductFinder\Controller\Test
 */
class ProductImportController extends AbstractController
{
    /**
     * @Route("/product-finder/test/product-import", name="@SwissbitProductFinder:Test:ProductImport[import]")
     * @param Request $request
     *
     * @return void
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \netshake\SwissbitProductFinder\Import\Exception
     */
    public function importAction( Request $request )
    {
        /** @var ProductImportService $productImportService */
        $productImportService = $this->container->get( ProductImportService::class );

        $sourceFile = $productImportService->getSourceFile();
        if( $sourceFile ) {
            $productImportService->import();

            $errors = $productImportService->getErrors();

            echo "<pre>";
            var_export( $errors );
            echo "</pre>";
        }

        die( 'asd' );
    }
}
