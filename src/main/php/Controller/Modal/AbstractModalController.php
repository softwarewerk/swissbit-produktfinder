<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 14:52 - 22.03.18
 */

namespace netshake\SwissbitProductFinder\Controller\Modal;

use netshake\SwissbitProductFinder\Controller\AbstractController;

/**
 * Class AbstractModalController
 *
 * @package netshake\SwissbitProductFinder\Controller\Modal
 */
abstract class AbstractModalController extends AbstractController
{

}
