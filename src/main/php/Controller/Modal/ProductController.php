<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 16:33 - 01.03.18
 */

namespace netshake\SwissbitProductFinder\Controller\Modal;

use netshake\SwissbitProductFinder\Di\Service\OctopartService;
use netshake\SwissbitProductFinder\Di\Service\ProductService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


/**
 * Class ProductController
 *
 * @package netshake\SwissbitProductFinder\Controller\Modal
 */
class ProductController extends AbstractModalController
{
    /**
     * @Route("/product-finder/modal/product/{id}", name="@SwissbitProductFinder:Modal:Product")
     * @param Request $request
     *
     * @return Response
     */
    public function indexController( Request $request )
    {
        /** @var OctopartService $octopartService */
        $octopartService = $this->container->get( OctopartService::class );

        /** @var ProductService $productService */
        $productService = $this->get( ProductService::class );

        $product                   = $productService->find( $request->get( 'id' ) );
        $manufacturerProductNumber = $product->getSwissbitPartNumber();

        $octopartService->setProductPartNumber( $manufacturerProductNumber )->query();

        return $this->render(
            '@SwissbitProductFinder/modal/product/index.twig', [
            'product' => $product,
            'buy_url' => $octopartService->getPreferedBuyUrl( $manufacturerProductNumber ),
            'offers'  => $octopartService->getItemOffers( $manufacturerProductNumber )
        ] );
    }
}
