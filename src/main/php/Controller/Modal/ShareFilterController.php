<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 11:48 - 06.03.18
 */

namespace netshake\SwissbitProductFinder\Controller\Modal;

use netshake\SwissbitProductFinder\Di\Service\ProductFilterService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ShareFilterController
 *
 * @package netshake\SwissbitProductFinder\Controller\Modal
 */
class ShareFilterController extends AbstractModalController
{
    /**
     * @var array
     */
    private $formErrorMessages = [
        'name'           => '',
        'message'        => '',
        'receiver_name'  => '',
        'receiver_email' => '',
    ];

    /**
     * @var array
     */
    private $formValues = [
        'name'           => '',
        'message'        => '',
        'receiver_name'  => '',
        'receiver_email' => '',
    ];

    /**
     * @param Request $request
     *
     * @return bool
     */
    private function isValid( Request $request )
    {
        $valid   = true;
        $name    = $request->get( 'Name' );
        $message = $request->get( 'Message' );

        $receiver_email = $request->get( 'ReceiverEmail' );
        $receiver_name  = $request->get( 'ReceiverName' );

        if( empty( $name ) ) {
            $valid                           = false;
            $this->formErrorMessages['name'] = 'Field is required and may not be empty.';
        }

        if( empty( $receiver_name ) ) {
            $valid                                    = false;
            $this->formErrorMessages['receiver_name'] = 'Receiver name is required.';
        }

        if( empty( $receiver_email ) ) {
            $valid                                     = false;
            $this->formErrorMessages['receiver_email'] = 'Receiver email is required.';
        }

        $this->formValues['name']    = $name;
        $this->formValues['message'] = $message;

        $this->formValues['receiver_name']  = $receiver_name;
        $this->formValues['receiver_email'] = $receiver_email;

        return $valid;
    }

    /**
     * /product-finder/product-filter/restore?pid=9&amp;fid=5abcd5e695d3c
     *
     * @Route("/product-finder/modal/share-filter", name="@SwissbitProductFinder:Modal:ShareFilter")
     * @param Request $request
     *
     * @return Response
     * @throws \netshake\SwissbitProductFinder\Exception
     */
    public function indexController( Request $request )
    {
        $email_content = null;
        if( Request::METHOD_POST == $request->getMethod() && $this->isValid( $request ) ) {
            /** @var ProductFilterService $productFilterService */
            $productFilterService = $this->get( ProductFilterService::class );

            $name          = $this->formValues['name'];
            $message       = $this->formValues['message'];
            $receiverName  = $this->formValues['receiver_name'];
            $receiverEmail = $this->formValues['receiver_email'];
            $mail          = $this->newEmail( 'Share Filter' ); // TODO: translate

//            try {
            $filterId = $productFilterService->restoreFiltersFromSession()
                                             ->storeFilter()
                                             ->getFilterId();
//            } catch( \Exception $e ) {
//                return 'error';
//            }

            $email_content = $this->renderView( "@SwissbitProductFinder/locale/de/mail/share-filter.twig", [
                'name'           => $name,
                'message'        => $message,
                'receiver_name'  => $receiverName,
                'receiver_email' => $receiverName,
                'share_url'      => $request->getSchemeAndHttpHost()
                                    . $this->generateUrl( '@SwissbitProductFinder:ProductFilter[restore]', [
                        'pid' => $request->get( 'pid' ),
                        'fid' => $filterId
                    ] )
            ] );

            $mail->text = $email_content;

            $mail->sendTo( "$receiverName <$receiverEmail>" );

            return $this->render( '@SwissbitProductFinder/modal/share-filter/success.twig' );
        }

        return $this->render(
            '@SwissbitProductFinder/modal/share-filter/index.twig', [
            'pid'            => $request->get( 'pid' ),
            'email_content'  => $email_content,
            'values'         => $this->formValues,
            'error_messages' => $this->formErrorMessages
        ] );
    }
}
