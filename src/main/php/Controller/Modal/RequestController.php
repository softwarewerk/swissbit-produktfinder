<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 11:48 - 06.03.18
 */

namespace netshake\SwissbitProductFinder\Controller\Modal;

use Dompdf\Dompdf;
use netshake\SwissbitProductFinder\Di\Service\DomPdfService;
use netshake\SwissbitProductFinder\Di\Service\ProductCompareService;
use netshake\SwissbitProductFinder\Di\Service\ProductService;
use netshake\SwissbitProductFinder\Entity\Product;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class RequestController
 *
 * @package netshake\SwissbitProductFinder\Controller\Modal
 */
class RequestController extends AbstractModalController
{
    const CONTEXT_DEFAULT = 'default';
    const CONTEXT_PRODUCT_COMPARE = 'product-compare';
    const CONTEXT_PRODUCT = 'product-compare';

    /**
     * @var array
     */
    private $formErrorMessages = [
        'name'    => '',
        'email'   => '',
        'message' => ''
    ];

    /**
     * @var array
     */
    private $formValues = [
        'name'    => '',
        'email'   => '',
        'message' => ''
    ];

    /**
     * @param Request $request
     *
     * @return bool
     */
    private function isValid( Request $request )
    {
        $valid   = true;
        $name    = $request->get( 'Name' );
        $email   = $request->get( 'Email' );
        $message = $request->get( 'Message' );

        if( empty( $name ) ) {
            $valid                           = false;
            $this->formErrorMessages['name'] = 'Field is required and may not be empty.';
        }
        if( empty( $email ) ) {
            $valid                            = false;
            $this->formErrorMessages['email'] = 'Field is required and may not be empty.';
        }

        $this->formValues['name']    = $name;
        $this->formValues['email']   = $email;
        $this->formValues['message'] = $message;

        return $valid;
    }

    /**
     * @return Dompdf
     */
    private function getProductComparePdf()
    {
        /** @var ProductCompareService $productCompareService */
        $productCompareService = $this->container->get( ProductCompareService::class );

        $productCompareService->restoreFromSession();

        if( count( $productCompareService->getAllProductIds() ) ) {
            /** @var DomPdfService $domPdfService */
            $domPdfService = $this->container->get( DomPdfService::class );

            // (Optional) Setup the paper size and orientation
            $domPdfService->setPaper( 'A4', 'landscape' );

            return $domPdfService->loadHtml( $this->renderView(
                '@SwissbitProductFinder/download/_dom-pdf/product-compare.twig',
                array( 'table' => $productCompareService->restoreFromSession()->createTableData() )
            ) )
                                 ->render();
        }

        return null;
    }

    /**
     * @return Dompdf
     */
    private function getProductPdf( Product $product )
    {
        /** @var DomPdfService $domPdfService */
        $domPdfService = $this->container->get( DomPdfService::class );

        // (Optional) Setup the paper size and orientation
        $domPdfService->setPaper( 'A4', 'landscape' );

        return $domPdfService->loadHtml( $this->renderView(
            '@SwissbitProductFinder/download/_dom-pdf/product.twig',
            [ 'item' => $product ]
        ) )
                             ->render();
    }

    /**
     * @Route("/product-finder/modal/request/",
     *     name="@SwissbitProductFinder:Modal:Request")
     * @param Request $request
     *
     * @return Response
     */
    public function requestAction( Request $request )
    {
        if( Request::METHOD_POST == $request->getMethod() && $this->isValid( $request ) ) {
//            $locale = strtolower( substr( $request->getLocale(), 0, 2 ) );
            $mail = $this->newEmail( 'Anfrage' );

            $mail->text = $this->renderView( "@SwissbitProductFinder/locale/de/mail/request.twig", [
                'data' => $this->formValues
            ] );

            $this->sendEmailToTeam( $mail );

            return $this->render( '@SwissbitProductFinder/modal/request/success.twig' );
        }

        return $this->render(
            '@SwissbitProductFinder/modal/request/request.twig', [
            'form_target'    => $this->generateUrl( '@SwissbitProductFinder:Modal:Request' ),
            'error_messages' => $this->formErrorMessages,
            'values'         => $this->formValues
        ] );
    }

    /**
     * @Route("/product-finder/modal/request/product-{id}",
     *     name="@SwissbitProductFinder:Modal:Request[product]")
     * @param Request $request
     *
     * @return Response
     */
    public function productRequestAction( Request $request )
    {
        if( Request::METHOD_POST == $request->getMethod() && $this->isValid( $request ) ) {
            /** @var ProductService $productService */
            $productService = $this->get( ProductService::class );
            $mail           = $this->newEmail( 'Anfrage' );
            $product        = $productService->find( $request->get( 'id' ) );
            $productPdf     = $this->getProductPdf( $product );

            if( $productPdf ) {
                $mail->attachFileFromString( $productPdf->output(), 'product.pdf' );
            }

            $mail->text = $this->renderView( "@SwissbitProductFinder/locale/de/mail/request-product.twig", [
                'data' => $this->formValues
            ] );

            $this->sendEmailToTeam( $mail );

            return $this->render( '@SwissbitProductFinder/modal/request/success.twig' );
        }

        return $this->render(
            '@SwissbitProductFinder/modal/request/request.twig', [
            'form_target'    => $this->generateUrl( '@SwissbitProductFinder:Modal:Request[product]', [
                'id' => $request->get( 'id' )
            ] ),
            'error_messages' => $this->formErrorMessages,
            'values'         => $this->formValues
        ] );
    }

    /**
     * @Route("/product-finder/modal/request/product-compare/",
     *     name="@SwissbitProductFinder:Modal:Request[product-compare]")
     * @param Request $request
     *
     * @return Response
     */
    public function productCompareRequestAction( Request $request )
    {
        if( Request::METHOD_POST == $request->getMethod() && $this->isValid( $request ) ) {
            $mail              = $this->newEmail( 'Anfrage' );
            $productComparePdf = $this->getProductComparePdf();

            if( $productComparePdf ) {
                $mail->attachFileFromString( $productComparePdf->output(), 'product-compare.pdf' );
            }

            $mail->text = $this->renderView(
                "@SwissbitProductFinder/locale/de/mail/request-product-compare.twig", [
                'data' => $this->formValues
            ] );

            $this->sendEmailToTeam( $mail );

            return $this->render( '@SwissbitProductFinder/modal/request/success.twig' );
        }


        return $this->render(
            '@SwissbitProductFinder/modal/request/request.twig', [
            'form_target'    => $this->generateUrl( '@SwissbitProductFinder:Modal:Request[product-compare]' ),
            'error_messages' => $this->formErrorMessages,
            'values'         => $this->formValues
        ] );
    }
}
