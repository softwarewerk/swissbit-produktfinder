<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 15:05 - 06.03.18
 */

namespace netshake\SwissbitProductFinder\Controller\Modal;

use netshake\SwissbitProductFinder\Di\Service\ProductCompareService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ProductCompareController
 *
 * @package netshake\SwissbitProductFinder\Controller\Modal
 */
class ProductCompareController extends AbstractModalController
{
    /**
     * @Route("/product-finder/modal/product-compare/", name="@SwissbitProductFinder:Modal:ProductCompare")
     * @param Request $request
     *
     * @return Response
     */
    public function indexController( Request $request )
    {
        /** @var ProductCompareService $productCompareService */
        $productCompareService = $this->container->get( ProductCompareService::class );

        $productCompareService->restoreFromSession();

        if( 'reset' == $request->get( 'do' ) ) {
            $productCompareService->clear()->rememberProducts();
        }

        if( 2 > $productCompareService->count() ) {
            return $this->render(
                '@SwissbitProductFinder/modal/product-compare/not-enough-products.twig',
                array( 'table' => $productCompareService->createTableData() )
            );
        }

        return $this->render(
            '@SwissbitProductFinder/modal/product-compare/index.twig',
            array( 'table' => $productCompareService->createTableData() )
        );
    }
}
