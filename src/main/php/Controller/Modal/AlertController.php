<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 15:22 - 06.03.18
 */

namespace netshake\SwissbitProductFinder\Controller\Modal;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AlertController
 *
 * @package netshake\SwissbitProductFinder\Controller\Modal
 */
class AlertController extends AbstractModalController
{
    /**
     * @Route("/product-finder/modal/alert/", name="@SwissbitProductFinder:Modal:Alert")
     * @param Request $request
     *
     * @return Response
     */
    public function indexController( Request $request )
    {
        return $this->render(
            '@SwissbitProductFinder/modal/alert/index.twig',
            array( 'message' => $request->get( 'message' ) )
        );
    }
}
