<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 12:54 - 07.03.18
 */

namespace netshake\SwissbitProductFinder\Controller\Admin\Modal;

use netshake\SwissbitProductFinder\Controller\Modal\AbstractModalController;
use netshake\SwissbitProductFinder\Di\Service\ProductImportService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class UploadProductsController
 *
 * @package netshake\SwissbitProductFinder\Controller\Admin\Modal
 */
class UploadProductsController extends AbstractModalController
{
    /**
     * @Route("/product-finder/admin/modal/import-products/",
     *     name="@SwissbitProductFinder:Admin:Modal:ImportProducts")
     * @param Request $request
     *
     * @return Response
     * @throws \netshake\SwissbitProductFinder\Import\Exception\FileUploadException
     * @throws \netshake\SwissbitProductFinder\Import\Exception
     */
    public function indexAction( Request $request )
    {
        /** @var ProductImportService $importService */
        $importService = $this->get( ProductImportService::class );

        if( Request::METHOD_POST == $request->getMethod() ) {
            $importService->uploadFile( 'File' );
        }

        return $this->render(
            '@SwissbitProductFinder/admin/modal/import-products/index.twig',
            array( 'sourcefile' => basename( $importService->getSourceFile() ) )
        );
    }

    /**
     * @Route("/product-finder/admin/modal/import-products/import",
     *     name="@SwissbitProductFinder:Admin:Modal:ImportProducts[import]")
     * @param Request $request
     *
     * @return Response
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \netshake\SwissbitProductFinder\Import\Exception
     */
    public function importAction( Request $request )
    {
        /** @var ProductImportService $importService */
        $importService = $this->get( ProductImportService::class );

        $ignoreInvalidEntries = (bool)$request->get( 'force', false );

        $importService->import( $ignoreInvalidEntries );

        if( $importService->isImportSuccess() ) {
            return $this->render(
                '@SwissbitProductFinder/admin/modal/import-products/import-success.twig',
                array(
                    'imported_entries' => $importService->getSuccessFullImportedEntries(),
                    'ignored_entries' => $importService->getInvalidEntries() )
            );
        }

        return $this->render(
            '@SwissbitProductFinder/admin/modal/import-products/import-summary.twig',
            array(
                'errors'              => $importService->getErrors(),
                'total_entries_found' => $importService->getTotalEntriesFound(),
                'valid_entries'       => $importService->getValidEntries(),
                'invalid_entries'     => $importService->getInvalidEntries()
            )
        );
    }

    /**
     * @Route("/product-finder/admin/modal/import-products/remove-source-file",
     *     name="@SwissbitProductFinder:Admin:Modal:ImportProducts[remove-source-file]")
     * @return Response
     * @throws \netshake\SwissbitProductFinder\Import\Exception
     */
    public function removeSourceFileAction()
    {
        /** @var ProductImportService $importService */
        $importService = $this->get( ProductImportService::class );

        $importService->removeSourceFile();

        return $this->redirectToRoute( '@SwissbitProductFinder:Admin:Modal:ImportProducts' );
    }

    /**
     * @Route("/product-finder/admin/modal/import-products/download-source-file",
     *     name="@SwissbitProductFinder:Admin:Modal:ImportProducts[download-import-file]")
     */
    public function downloadImportFileAction()
    {
        /** @var ProductImportService $importService */
        $importService = $this->get( ProductImportService::class );

        $sourceFile = $importService->getSourceFile();

        if( $sourceFile ) {
            header( 'Content-Description: File Transfer' );
            header( 'Content-Type: application/octet-stream' );
            header( 'Content-Disposition: attachment; filename="' . basename( $sourceFile ) . '"' );
            header( 'Expires: 0' );
            header( 'Cache-Control: must-revalidate' );
            header( 'Pragma: public' );
            header( 'Content-Length: ' . filesize( $sourceFile ) );
            flush(); // Flush system output buffer
            readfile( $sourceFile );
        }

        exit;
    }
}
