<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 18:53 - 28.03.18
 */

namespace netshake\SwissbitProductFinder\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AbstractController extends Controller
{
    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    /**
     * @return void
     */
    public function init()
    {
    }

    /**
     * @return \Swift_Mailer
     */
    protected function getMailer()
    {
        if( ! isset( $this->mailer ) ) {
            $this->mailer = \System::getContainer()->get( 'swiftmailer.mailer' );
        }

        return $this->mailer;
    }

    /**
     * @param string $subject
     *
     * @return \Contao\Email
     */
    protected function newEmail( $subject )
    {
        $mail = new \Contao\Email( $this->getMailer() );

        $senderEmail = \Contao\Config::get( 'swissbit_product-finder_mail_sender_email' );
        $senderName  = \Contao\Config::get( 'swissbit_product-finder_mail_sender_name' );

        if( empty( $senderEmail ) ) {
            throw new Exception( 'Unable to preapre E-Mail. Missing sender email.' );
        }

        $mail->from     = $senderEmail;
        $mail->fromName = $senderName;

        if( ! empty( $subject ) ) {
            $mail->subject = $subject;
        }

        return $mail;
    }

    /**
     * @param \Contao\Email $email
     *
     * @return $this
     */
    protected function sendEmailToTeam( \Contao\Email $email )
    {
        $emails = $this->getTeamEmails();
        if( ! empty( $emails ) ) {
            call_user_func_array( array( $email, 'sendTo' ), $emails );
        }

        return $this;
    }

    /**
     * @return array
     */
    protected function getTeamEmails()
    {
        $teamEmailsSetting = \Contao\Config::get( 'swissbit_product-finder_mail_team' );

        if( ! empty( $teamEmailsSetting ) ) {
            return array_map( 'trim', explode( ',', $teamEmailsSetting ) );
        }

        return [];
    }
}
