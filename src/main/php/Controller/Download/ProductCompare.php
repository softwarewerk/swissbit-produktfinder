<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 09:11 - 29.03.18
 */

namespace netshake\SwissbitProductFinder\Controller\Download;

use netshake\SwissbitProductFinder\Controller\AbstractController;
use netshake\SwissbitProductFinder\Di\Service\DomPdfService;
use netshake\SwissbitProductFinder\Di\Service\ProductCompareService;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ProductCompare
 *
 * @package netshake\SwissbitProductFinder\Controller\Download
 */
class ProductCompare extends AbstractController
{
    /**
     * @Route("/product-finder/download/product-compare/swissbit-compare.pdf",
     *     name="@SwissbitProductFinder:Download:ProductCompare[download-pdf]")
     * @param Request $request
     *
     * @return void
     */
    public function downloadPdfAction( Request $request )
    {
        /** @var ProductCompareService $productCompareService */
        $productCompareService = $this->container->get( ProductCompareService::class );

        /** @var DomPdfService $domPdfService */
        $domPdfService = $this->container->get( DomPdfService::class );

        // (Optional) Setup the paper size and orientation
        $domPdfService->setPaper( 'A4', 'landscape' );

        $domPdfService->loadHtml( $this->renderView(
            '@SwissbitProductFinder/download/_dom-pdf/product-compare.twig',
            array( 'table' => $productCompareService->restoreFromSession()->createTableData() )
        ) )
                      ->render()
                      ->stream( 'swissbit-compare.pdf', array( 'Attachment' => 0 ) );
        exit;
    }

    /**
     * @Route("/product-finder/product-compare/compare.xls",
     *     name="@SwissbitProductFinder:Download:ProductCompare[download-xls]")
     * @param Request $request
     *
     * @return void
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function downloadXlsAction( Request $request )
    {
        /** @var ProductCompareService $productCompareService */
        $productCompareService = $this->container->get( ProductCompareService::class );

        $productCompareTableData = $productCompareService->restoreFromSession()->createTableData();
        $titles                  = $productCompareTableData['titles'];
        $body                    = $productCompareTableData['body'];

        /** Create a new Spreadsheet Object **/
        $spreadsheet = new Spreadsheet();
        $spreadsheet->getProperties()
                    ->setCreator( "Swissbit" )
                    ->setLastModifiedBy( "Swissbit" )
                    ->setTitle( "Swissbit Product-Compare Result" )
                    ->setSubject( "Swissbit Product-Compare Result" )
                    ->setDescription(
                        "Swissbit Product-Compare Result"
                    )
                    ->setKeywords( "swissbit product-compare" )
                    ->setCategory( "swissbit" );

        $workSheet = $spreadsheet->getActiveSheet();

        $startColumNumber = 2;
        foreach( $titles as $title ) {
            $workSheet->getCellByColumnAndRow( $startColumNumber ++, 1 )
                      ->setValue( $title )
                      ->getStyle()
                      ->getFont()
                      ->setBold( true );
        }

        $startRowNumber = 3;
        foreach( $body as $row ) {
            $title            = $row['title'];
            $cells            = $row['cells'];
            $startColumNumber = 1;

            $workSheet->getCellByColumnAndRow( 1, $startRowNumber )
                      ->setValue( $title )
                      ->getStyle()
                      ->getFont()
                      ->setBold( true );

            foreach( $cells as $cell ) {
                $value = $cell['value'];
                $type  = $cell['type'];

                if( 'image' == $type ) {
                    $workSheet->getCellByColumnAndRow( ++ $startColumNumber, $startRowNumber )
                              ->setValue( $value )
                              ->getHyperlink()
                              ->setUrl( $value );
                } else {
                    $workSheet->setCellValueByColumnAndRow( ++ $startColumNumber, $startRowNumber, $value );
                }
            }

            $startRowNumber ++;
        }

        $workSheet->refreshColumnDimensions();

        foreach( $workSheet->getColumnIterator() as $column ) {
//            $workSheet->refreshColumnDimensions();
            $workSheet->getColumnDimension( $column->getColumnIndex() )->setAutoSize( true );
        }

        $writer = IOFactory::createWriter( $spreadsheet, "Xls" );

        // Redirect output to a client’s web browser (Excel5)
        header( 'Content-Type: application/vnd.ms-excel' );
        header( 'Content-Disposition: attachment;filename="Swissbit Product-Compare Result.xls"' );
        header( 'Cache-Control: max-age=0' );

        $writer->save( 'php://output' );

        exit;
    }
}
