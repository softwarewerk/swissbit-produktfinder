<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 09:11 - 29.03.18
 */

namespace netshake\SwissbitProductFinder\Controller\Download;

use netshake\SwissbitProductFinder\Controller\AbstractController;
use netshake\SwissbitProductFinder\Di\Service\DomPdfService;
use netshake\SwissbitProductFinder\Di\Service\ProductService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class Product
 *
 * @package netshake\SwissbitProductFinder\Controller\Download
 */
class Product extends AbstractController
{
    /**
     * @Route("/product-finder/download/product-{id}.pdf",
     *     name="@SwissbitProductFinder:Download:Product[download-pdf]")
     * @param Request $request
     *
     * @return void
     */
    public function downloadPdfAction( Request $request )
    {
        /** @var ProductService $productService */
        $productService = $this->container->get( ProductService::class );

        /** @var DomPdfService $domPdfService */
        $domPdfService = $this->container->get( DomPdfService::class );

        $domPdfService->loadHtml( $this->renderView(
            '@SwissbitProductFinder/download/_dom-pdf/product.twig',
            array( 'item' => $productService->find( $request->get( 'id' ) ) )
        ) )
                      ->render()
                      ->stream( 'product.pdf', array( 'Attachment' => 0 ) );
        exit;
    }
}
