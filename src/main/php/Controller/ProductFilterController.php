<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 17:01 - 05.02.18
 */

namespace netshake\SwissbitProductFinder\Controller;

use netshake\SwissbitProductFinder\Di\Service\ProductFilterService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;


/**
 * Class ProductFilterController
 *
 * @package netshake\SwissbitProductFinder\Controller
 */
class ProductFilterController extends AbstractController
{
    /**
     * @Route("/product-finder/use-case/select/{use_case}",
     *     name="@SwissbitProductFinder:ProductFilter[use-case-add]")
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function useCaseAddAction( Request $request )
    {
        $selectedUseCase = $request->get( 'use_case' );

        /** @var ProductFilterService $productFilterService */
        $productFilterService = $this->container->get( ProductFilterService::class );
        $productFilterService->setUseCase( $selectedUseCase )->rememberFilterData();

        $callBackUrl = urldecode( $request->get( 'cb' ) );

        return $this->redirect( $callBackUrl );
    }

    /**
     * @Route("/product-finder/use-case/remove/{use_case}",
     *     name="@SwissbitProductFinder:ProductFilter[use-case-remove]")
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function useCaseRemoveAction( Request $request )
    {
        $useCase = $request->get( 'use_case' );

        /** @var ProductFilterService $productFilterService */
        $productFilterService = $this->container->get( ProductFilterService::class );
        $productFilterService->removeUseCase( $useCase );

        $callBackUrl = urldecode( $request->get( 'cb' ) );

        return $this->redirect( $callBackUrl );
    }

    /**
     * @Route("/product-finder/product-filter/restore",
     *     name="@SwissbitProductFinder:ProductFilter[restore]")
     * @param Request $request
     *
     * @return RedirectResponse
     * @throws \netshake\SwissbitProductFinder\Exception
     */
    public function restoreAction( Request $request )
    {
        /** @var ProductFilterService $productFilterService */
        $productFilterService = $this->get( ProductFilterService::class );

        $productFilterService->restoreFilter( $request->get( 'fid' ) )->rememberFilterData();

        $objPage = \Contao\PageModel::findByPK( $request->get( 'pid' ) );

        return $this->redirect( rtrim( /* just in case */
                                    $request->getBaseUrl(), '/' )
                                . '/'
                                . $objPage->getFrontendUrl() );
    }
}
