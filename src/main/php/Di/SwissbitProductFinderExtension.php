<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 17:38 - 05.02.18
 */

namespace netshake\SwissbitProductFinder\Di;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

/**
 * Class SwissbitProductFinderExtension
 *
 * @package netshake\SwissbitProductFinder\Di
 */
class SwissbitProductFinderExtension extends Extension
{
    /**
     * Loads a specific configuration.
     *
     * @param array            $configs
     * @param ContainerBuilder $container
     */
    public function load( array $configs, ContainerBuilder $container )
    {
        $loader = new YamlFileLoader(
            $container,
            new FileLocator( __DIR__ . '/../Resources/config' )
        );
        $loader->load( 'services.yaml' );

        $this->addAnnotatedClassesToCompile( array(
            'netshake\\SwissbitProductFinder',
//            \netshake\SwissbitProductFinder\Controller\UseCaseController::class
        ) );
    }
}
