<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 13:28 - 12.02.18
 */

namespace netshake\SwissbitProductFinder\Di\Service;

use netshake\SwissbitProductFinder\Entity\Product;

/**
 * Class ProductAvailabilityService
 *
 * @see {https://octopart.com/api/home}
 *
 * @package netshake\SwissbitProductFinder\Di\Service
 */
class ProductAvailabilityService
{
    /**
     * @var Product[]
     */
    private $products;

    /**
     * @param Product[] $products
     *
     * @return $this
     */
    public function setProducts( $products )
    {
        $this->products = $products;

        return $this;
    }

    /**
     * @return bool
     */
    public function isAvailable()
    {
        return false;
    }

    /**
     * @return $this
     */
    public function obtainProductAvailabilities()
    {
        return $this;
    }

}
