<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 11:44 - 22.03.18
 */

namespace netshake\SwissbitProductFinder\Di\Service;

use Dompdf\Canvas;
use Dompdf\Dompdf;

/**
 * Class DomPdfService
 *
 * @package netshake\SwissbitProductFinder\Di\Service
 */
class DomPdfService extends Dompdf
{
    const EVENT_BEGIN_PAGE_RENDER = 'begin_page_render';
    const EVENT_END_PAGE_RENDER = 'end_page_render';

    /**
     * @var GeneralService
     */
    private $generalService;

    /**
     * @var int
     */
    private $pageOffset = 0;

    /**
     * @var array
     */
    private $registeredEventHandler = array();

    /**
     * Netshake_KelchCatalog_Model_DomPdf constructor.
     *
     * @param array $options
     */
    public function __construct( GeneralService $generalService, array $options = null )
    {
        $this->generalService = $generalService;

        $baseDir             = $generalService->getApplicationHomeDirectory() . DIRECTORY_SEPARATOR . 'dom-pdf';
        $domPdfTempDir       = $baseDir . DIRECTORY_SEPARATOR . 'temp';
        $domPdfFontDir       = $baseDir . DIRECTORY_SEPARATOR . 'font-dir';
        $domPdfFontsCacheDir = $baseDir . DIRECTORY_SEPARATOR . 'cache';

        $domPdfConfigParams = array(
            'is_php_enabled'    => false,
            'temp_dir'          => $domPdfTempDir,
            'is_remote_enabled' => true,
            'font_dir'          => $domPdfFontDir,
            'font_cache'        => $domPdfFontsCacheDir
        );

        if( null != $options ) {
            $domPdfConfigParams = array_merge( $domPdfConfigParams, $options );
        }

        parent::__construct( $domPdfConfigParams );

//        $this->setCallbacks( array(
//            array( 'event' => 'begin_page_render', 'f' => array( $this, 'triggerEvent' ) ),
//            array( 'event' => 'end_page_render', 'f' => array( $this, 'triggerEvent' ) )
//        ) );
    }

    /**
     * @param string   $eventName
     * @param callable $callback
     *
     * @return $this
     */
    public function registerEventHandler( $eventName, $callback )
    {
        array_push( $this->registeredEventHandler, array( 'event' => $eventName, 'f' => $callback ) );

        return $this;
    }

    /**
     * @return array
     */
    public function getRegisteredEventHandlers()
    {
        return $this->registeredEventHandler;
    }

    /**
     * @param int $offset
     *
     * @return $this
     */
    public function setPageOffset( $offset )
    {
        $this->pageOffset = $offset;

        return $this;
    }

    /**
     * @return int
     */
    public function getPageOffset()
    {
        return $this->pageOffset;
    }

    /**
     * @param string $str
     * @param string $encoding
     *
     * @return $this
     */
    public function loadHtml( $str, $encoding = 'UTF-8' )
    {
        parent::loadHtml( $str, $encoding );

        return $this;
    }

    /**
     * Aufrufen der PHP-Skripte.
     * Wird nur ausgeführt wenn der Parameter is_php_enabled => true ist
     *
     * @param Canvas $pdf
     * @param int    $pageNum
     * @param int    $pageCount
     */
    public function runPageScripts( Canvas $pdf, $pageNum, $pageCount )
    {

    }

//    /**
//     * @param array $event ['canvas':\Dompdf\Canvas, 'frame':\Dompdf\FrameDecorator\Block]
//     *
//     * @return void
//     */
//    public final function triggerEventOnPageRender( $event )
//    {
//    }

//    /**
//     * @param array $event ['canvas':\Dompdf\Canvas, 'frame':\Dompdf\FrameDecorator\Block]
//     *
//     * @return void
//     */
//    public function onEndPageRender( $event )
//    {
//        /** @var \Dompdf\Canvas $canvas */
//        $canvas = $event['canvas'];
//
//        $font     = $this->getFontMetrics()->getFont( "Arial, Helvetica, sans-serif", "normal" );
//        $size     = 12;
//        $pageText = "Page: " . ( $canvas->get_page_number() + $this->pageNumOffset );
//        $y        = 15;
//        $x        = 520;
//        $canvas->text( $x, $y, $pageText, $font, $size );
//    }

    /**
     * @return $this
     */
    public function render()
    {
        foreach(
            array(
                $this->getOptions()->get( 'temp_dir' ),
                $this->getOptions()->get( 'font_dir' ),
                $this->getOptions()->get( 'font_cache' )
            ) as $path
        ) {
            if( ! is_dir( $path ) ) {
                mkdir( $path, 0755, true );
            }
        }

        $this->setCallbacks( $this->getRegisteredEventHandlers() );

        parent::render();

        $this->add_info( "Producer", 'swissbit' );
        $this->add_info( "Creator", 'swissbit' );

        return $this;
    }
}
