<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 13:28 - 12.02.18
 */

namespace netshake\SwissbitProductFinder\Di\Service;

use Contao\Config;

/**
 * Class OctopartService
 *
 * @see     {https://octopart.com/api/home}
 *
 * @package netshake\SwissbitProductFinder\Di\Service
 */
class OctopartService
{
    private $partNumbers = array();

    /**
     * @var array
     */
    private $results;

    /**
     * @var array
     */
    private $request;

    /**
     * @var array
     */
    private $itemsOffers = array();

    /**
     * @var array
     */
    private $itemsBuyUrls = array();

    /**
     * @var array
     */
    private $itemsTotalQuantities = array();

    /**
     * @return $this
     */
    public function query()
    {
        /*
         * Reset first
         */

        unset( $this->request );
        $this->results = array();

        $this->itemsOffers          = array();
        $this->itemsTotalQuantities = array();
        $this->itemsBuyUrls         = array();

        if( empty( $this->partNumbers ) ) {
            return $this;
        }

        /*
        curl -G http://octopart.com/api/v3/parts/match \
           -d queries="[{\"mpn\":\"SFCF1024H1BK2TO-C-MS-553-SMA\"}]" \
           -d apikey=EXAMPLE_KEY \
           -d pretty_print=true
         */

        // Get rid of duplicates
        $partNumbers = array_flip( array_flip( $this->partNumbers ) );

        $request_param_queries = [];
        foreach( $partNumbers as $partNumber ) {
            $request_param_queries[] = [ 'mpn' => $partNumber, 'reference' => $partNumber ];
        }

        $request_param_queries_json = json_encode( $request_param_queries );
        $request_params             = [
            'apikey'       => Config::get( 'swissbit_product-finder_octopart_api_key' )
//            'pretty_print' => 'false'
        ];
        $query                      = http_build_query( $request_params ) . "&queries={$request_param_queries_json}";

        $url = Config::get( 'swissbit_product-finder_octopart_api_base_url' ) . "/parts/match?{$query}";

        $ch = curl_init( $url );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, 0 );
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0 );

        $output = curl_exec( $ch );
        $errors = curl_error( $ch );
        curl_close( $ch );

        $response = json_decode( $output, true );

        if( empty( $response ) ) {
//            var_dump( $errors );
            return $this;
        }

        // var_dump( $response['results'][0]['items'] );

        $this->request = $response['request'];
        $this->results = $response['results'];

        $allItemOffers        = array();
        $itemsTotalQuantities = array();
        $allItemsBuyUrls      = array();

        foreach( (array)$this->results as $result ) {
            $items     = $result['items'];
            $reference = $result['reference'];
            $hits      = $result['hits'];
            $error     = $result['error'];

            foreach( $items as $item ) {
                $itemPartNumber = $item['mpn'];
                $offers         = $item['offers'];

                $itemOffers        = [];
                $itemTotalQuantity = 0;
                $itemBuyUrls       = [];

                foreach( $offers as $offer ) {
                    $url             = $offer['product_url'];
                    $sellerInfo      = $offer['seller'];
                    $inStockQuantity = $offer['in_stock_quantity'];
                    $sellerName      = $sellerInfo['name'];

                    if( 0 < $inStockQuantity ) {
                        $itemBuyUrls[] = $url;
                    }

                    $itemOffers[] = [
                        'mpn'         => $itemPartNumber,
                        'url'         => $url,
                        'qty'         => $inStockQuantity,
                        'seller_name' => $sellerName,
                        'seller_info' => $sellerInfo
                    ];

                    $itemTotalQuantity += $inStockQuantity;
                }

                if( empty( $itemOffers ) ) {
                    continue;
                }

                if( ! isset( $resultOffers[$itemPartNumber] ) ) {
                    $allItemOffers[$itemPartNumber] = $itemOffers;
                } else {
                    $allItemOffers[$itemPartNumber] = array_merge( $allItemOffers[$itemPartNumber], $itemOffers );
                }

                if( ! isset( $itemsTotalQuantities[$itemPartNumber] ) ) {
                    $itemsTotalQuantities[$itemPartNumber] = $itemTotalQuantity;
                } else {
                    $itemsTotalQuantities[$itemPartNumber] += $itemTotalQuantity;
                }

                if( ! isset( $allItemsBuyUrls[$itemPartNumber] ) ) {
                    $allItemsBuyUrls[$itemPartNumber] = $itemBuyUrls;
                } else {
                    $allItemsBuyUrls[$itemPartNumber] = array_merge( $allItemsBuyUrls[$itemPartNumber], $itemBuyUrls );
                }
            }
        }

        $this->itemsOffers          = $allItemOffers;
        $this->itemsTotalQuantities = $itemsTotalQuantities;
        $this->itemsBuyUrls         = $allItemsBuyUrls;

        return $this;
    }

    /**
     * @param string $manufacturerProductPartNumber
     *
     * @return $this
     */
    public function addProductPartNumber( $manufacturerProductPartNumber )
    {
        $this->partNumbers[] = $manufacturerProductPartNumber;

        return $this;
    }

    /**
     * @param $manufacturerProductPartNumber
     *
     * @return $this
     */
    public function setProductPartNumber( $manufacturerProductPartNumber )
    {
        $this->partNumbers = [ $manufacturerProductPartNumber ];

        return $this;
    }

    /**
     * @param array $manufacturerProductPartNumbers
     *
     * @return $this
     */
    public function setProductPartNumbers( array $manufacturerProductPartNumbers )
    {
        $this->partNumbers = $manufacturerProductPartNumbers;

        return $this;
    }

    /**
     * @param string $manufacturerProductPartNumber
     *
     * @return array|string
     */
    public function getPreferedBuyUrl( $manufacturerProductPartNumber )
    {
        if( isset( $this->itemsBuyUrls[$manufacturerProductPartNumber] ) ) {
            $buyUrls = $this->itemsBuyUrls[$manufacturerProductPartNumber];

            if( ! empty( $buyUrls ) ) {
                return $buyUrls[array_rand( $buyUrls )];
            }
        }

        return null;
    }

    /**
     * @param string $manufacturerProductPartNumber
     *
     * @return array
     */
    public function getItemOffers( $manufacturerProductPartNumber = null )
    {
        if( empty( $manufacturerProductPartNumber ) ) {
            return $this->itemsOffers;
        }

        if( isset( $this->itemsOffers[$manufacturerProductPartNumber] ) ) {
            return $this->itemsOffers[$manufacturerProductPartNumber];
        }

        return null;
    }

    /**
     * @param string $manufacturerProductPartNumber
     *
     * @return array|int
     */
    public function getItemsTotalQuantities( $manufacturerProductPartNumber = null )
    {
        if( empty( $manufacturerProductPartNumber ) ) {
            return $this->itemsTotalQuantities;
        }

        if( isset( $this->itemsTotalQuantities[$manufacturerProductPartNumber] ) ) {
            return $this->itemsTotalQuantities[$manufacturerProductPartNumber];
        }

        return null;
    }

    /**
     * @param string $manufacturerProductPartNumber
     *
     * @return array|float
     */
    public function getItemsAvailabilityRating( $manufacturerProductPartNumber = null )
    {
        $rating = array_map( function( $qty ) {
            $rating = 0;
            if( 0 == $qty ) {
                $rating = 0;
            } else if( $qty < 50 ) {
                $rating = 0.5;
            } else if( $qty < 100 ) {
                $rating = 1;
            } else if( $qty < 200 ) {
                $rating = 1.5;
            } else if( $qty < 300 ) {
                $rating = 2;
            } else if( $qty < 400 ) {
                $rating = 2.5;
            } else if( $qty < 500 ) {
                $rating = 3;
            } else if( $qty < 600 ) {
                $rating = 3.5;
            } else if( $qty < 600 ) {
                $rating = 4;
            } else if( $qty < 800 ) {
                $rating = 4.5;
            } else {
                $rating = 5;
            }

            return $rating;
        }, $this->itemsTotalQuantities );

        if( empty( $manufacturerProductPartNumber ) ) {
            return $rating;
        }

        if( isset( $rating[$manufacturerProductPartNumber] ) ) {
            return $rating[$manufacturerProductPartNumber];
        }

        return null;
    }
}
