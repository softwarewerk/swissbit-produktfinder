<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 13:09 - 29.03.18
 */

namespace netshake\SwissbitProductFinder\Di\Service;

/**
 * Class GeneralService
 *
 * @package netshake\SwissbitProductFinder\Di\Service
 */
class GeneralService
{
    /**
     * @var string
     */
    private $homeDirectory;

    /**
     * @return string
     */
    public function getApplicationHomeDirectory()
    {
        if( ! isset( $this->homeDirectory ) ) {
            $this->homeDirectory = implode( DIRECTORY_SEPARATOR, [ sys_get_temp_dir(), 'swissbit' ] );
        }

        return $this->homeDirectory;
    }
}
