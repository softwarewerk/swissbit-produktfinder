<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 15:09 - 08.02.18
 */

namespace netshake\SwissbitProductFinder\Di\Service;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\ResultSetMapping;
use netshake\SwissbitProductFinder\Entity\Product;
use netshake\SwissbitProductFinder\Exception;
use netshake\SwissbitProductFinder\Import\Exception\FileUploadException;
use netshake\SwissbitProductFinder\Import\Validator\AbstractValidator;
use netshake\SwissbitProductFinder\Import\Validator\NotEmpty;
use netshake\SwissbitProductFinder\Import\Validator\NumericValidator;
use netshake\SwissbitProductFinder\Import\Validator\SwissbitPartNumberValidator;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx as SpreadsheetReader;

/**
 * Class ProductImportService
 *
 * @package netshake\SwissbitProductFinder\Di\Service
 */
class ProductImportService
{
    const IMPORT_INFO_SOURCE_FILE = 'source_file';

    const VALIDATORS = [
        'description'         => [
            [
                'class' => NotEmpty::class,
                'name'  => 'Description',
            ]
        ],
        'swissbitPartNumber'  => [
            [
                'class'  => SwissbitPartNumberValidator::class,
                'name'   => 'Swissbit Part Number',
                'params' => array( 'foo' => 'abr' )
            ]
        ],

        // Use-Cases
        'smallDataLogging'    => [
            [
                'class'    => NumericValidator::class,
                'name'     => 'Small Data Logging',
                'interval' => [ 0, 10 ]
            ]
        ],
        'largeDataLogging'    => [
            [
                'class'    => NumericValidator::class,
                'name'     => 'Large Data Logging',
                'interval' => [ 0, 10 ]
            ]
        ],
        'audioVideoRecording' => [
            [
                'class'    => NumericValidator::class,
                'name'     => 'Audio Video Recording',
                'interval' => [ 0, 10 ]
            ]
        ],
        'bootOnly'            => [
            [
                'class'    => NumericValidator::class,
                'name'     => 'Boot Only',
                'interval' => [ 0, 10 ]
            ]
        ],
        'bootVideoPlay'       => [
            [
                'class'    => NumericValidator::class,
                'name'     => 'Boot & Video Play',
                'interval' => [ 0, 10 ]
            ]
        ],
        'bootExec'            => [
            [
                'class'    => NumericValidator::class,
                'name'     => 'Boot Exec',
                'interval' => [ 0, 10 ]
            ]
        ],
        'useCase7'            => [
            [
                'class'    => NumericValidator::class,
                'name'     => 'Use Case 7',
                'interval' => [ 0, 10 ]
            ]
        ],
        'useCase8'            => [
            [
                'class'    => NumericValidator::class,
                'name'     => 'Use Case 8',
                'interval' => [ 0, 10 ]
            ]
        ],
    ];

    /**
     * @var array
     */
    const COLS = [
        "A"  => 'status',
        "B"  => 'type',
        "C"  => 'series',
        "D"  => 'family',
        "E"  => 'density',
        "F"  => 'densityClass',
        "G"  => 'swissbitPartNumber',
        "H"  => 'description',
        "I"  => 'temperatureGrade',
        "J"  => 'flashType',
        "K"  => 'forNewDesign',
        "L"  => 'factSheetWeblink',
        "M"  => 'productPictureWeblink',
        "N"  => 'endurance',
        "O"  => 'relCost',
        "P"  => 'seqReadPerformance',
        "Q"  => 'seqWritePerformance',
        "R"  => 'randReadPerformance',
        "S"  => 'randWritePerformance',
        "T"  => 'smallDataLogging',
        "U"  => 'largeDataLogging',
        "V"  => 'audioVideoRecording',
        "W"  => 'bootOnly',
        "X"  => 'bootVideoPlay',
        "Y"  => 'bootExec',
        "Z"  => 'useCase7',
        "AA" => 'useCase8'
    ];

    /**
     * @var GeneralService
     */
    private $generalService;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var array
     */
    private $importInfo;

    /**
     * @var string
     */
    private $sourceFile;

    /**
     * @var AbstractValidator[]
     */
    private $validatorInstances = [];

    /**
     * @var array
     */
    private $errors = [];

    /**
     * @var boolean
     */
    private $importSuccess = false;

    /**
     * @var int
     */
    private $totalEntriesFound = 0;

    /**
     * @var int
     */
    private $validEntries = 0;

    /**
     * @var int
     */
    private $invalidEntries = 0;

    /**
     * @var int
     */
    private $successFullImportedEntries = 0;

    /**
     * ProductImportService constructor.
     *
     * @param GeneralService $generalService
     * @param EntityManager  $entityManager
     */
    public function __construct( GeneralService $generalService, EntityManager $entityManager )
    {
        $this->generalService = $generalService;
        $this->entityManager  = $entityManager;
    }

    /**
     * @return bool
     */
    public function canImport()
    {
        return ! empty( $this->getSourceFile() );
    }

    /**
     * @return $this
     * @throws Exception
     */
    public function saveImportInfo()
    {
        if( ! isset( $this->importInfo ) ) {
            return $this;
        }

        $importInfoFile = $this->generalService->getApplicationHomeDirectory() . DIRECTORY_SEPARATOR . '.swissbit';
        if( ! file_put_contents( $importInfoFile, serialize( $this->importInfo ) ) ) {
            throw new Exception( "Failed to save import info. Insufficient permissions?" );
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function restoreImportInfo()
    {
        if( ! isset( $this->importInfo ) ) {
            $importInfoFile = $this->generalService->getApplicationHomeDirectory() . DIRECTORY_SEPARATOR . '.swissbit';
            if( ! is_file( $importInfoFile ) ) {
                $this->importInfo = [];

                return $this;
            }

            $this->importInfo = (array) @unserialize( file_get_contents( $importInfoFile ) );
        }

        return $this;
    }

    /**
     * @param string $name
     * @param mixed  $default
     *
     * @return mixed|null
     */
    public function getImportInfo( $name, $default = null )
    {
        if( ! isset( $this->importInfo ) ) {
            $this->restoreImportInfo();
        }

        if( isset( $this->importInfo[$name] ) ) {
            return $this->importInfo[$name];
        }

        return $default;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function removeImportInfo( $name )
    {
        if( isset( $this->importInfo[$name] ) ) {
            unset( $this->importInfo[$name] );
        }

        return $this;
    }

    /**
     * @param string $name
     * @param mixed  $value
     *
     * @return $this
     */
    public function setImportInfo( $name, $value )
    {
        if( ! isset( $this->importInfo ) ) {
            $this->restoreImportInfo();
        }

        $this->importInfo[$name] = $value;

        return $this;
    }

    /**
     * @param string $file [optional]
     *
     * @return ProductImportService
     */
    public function setSourceFile( $file = null )
    {
        if( null == $file ) {
            $file = $this->getImportInfo( self::IMPORT_INFO_SOURCE_FILE );
            if( empty( $file ) ) {
                return $this;
            }
        }

        $this->sourceFile = $file;

        return $this;
    }

    /**
     * @return string
     */
    public function getSourceFile()
    {
        if( ! isset( $this->sourceFile ) ) {
            $this->setSourceFile();
        }

        return $this->sourceFile;
    }

    /**
     * @return $this
     * @throws Exception
     */
    public function removeSourceFile()
    {
        $uploadedFile = $this->getImportInfo( self::IMPORT_INFO_SOURCE_FILE );
        if( is_file( $uploadedFile ) ) {
            if( ! unlink( $uploadedFile ) ) {
                trigger_error( sprintf( "Failed to delete file %s. Insufficient permissions?", $uploadedFile ),
                    E_USER_WARNING );
            }

            $this->removeImportInfo( self::IMPORT_INFO_SOURCE_FILE )->saveImportInfo();
        }

        return $this;
    }

    /**
     * @param string $name
     *
     * @return $this
     * @throws Exception
     * @throws FileUploadException
     */
    public function uploadFile( $name )
    {
        if( ! empty( $_FILES ) && isset( $_FILES[$name] ) ) {
            $fileInfo = $_FILES['File'];

            $fileName = $fileInfo['name'];
            $tempName = $fileInfo['tmp_name'];
            $error    = $fileInfo['error'];

            $ext = pathinfo( $fileName, PATHINFO_EXTENSION );

            if( ! in_array( $ext, array( 'xls', 'xlsx' ) ) ) {
                throw new FileUploadException( sprintf( "Unsupported extension %s.", $ext ) );
            }

            if( UPLOAD_ERR_OK == $error ) {
                $targetDir  = $this->generalService->getApplicationHomeDirectory();
                $targetFile = $targetDir . DIRECTORY_SEPARATOR . $fileName;

                if( ! is_dir( $targetDir ) ) {
                    if( ! mkdir( $targetDir, 0775, true ) ) {
                        throw new FileUploadException( sprintf(
                            "Failed to create target directory %s.", $targetDir ) );
                    }
                } else {
                    foreach( scandir( $targetDir ) as $fileName ) {
                        if( '.' == $fileName{0} ) {
                            continue;
                        }

                        $file = $targetDir . DIRECTORY_SEPARATOR . $fileName;

                        if( is_dir( $file ) ) {
                            continue;
                        }

                        if( ! unlink( $file ) ) {
                            trigger_error( "Failed to cleanup target directory. Insufficient permissions?",
                                E_USER_WARNING );
                        }
                    }
                }

                move_uploaded_file( $tempName, $targetFile );

                $this->setImportInfo( self::IMPORT_INFO_SOURCE_FILE, $targetFile )->saveImportInfo();

                return $this;
            }

            throw new FileUploadException( sprintf( "Upload of file name '%s' failed.", $name ), $error );
        }

        throw new FileUploadException( sprintf( "No uploaded file by name '%s' was found", $name ) );
    }

    /**
     * @param string $fieldName
     *
     * @return AbstractValidator[]
     * @throws Exception
     */
    private function getValidators( $fieldName )
    {
        if( ! isset( $this->validatorInstances[$fieldName] ) ) {
            $validators     = array();
            $validatorSpecs = isset( self::VALIDATORS[$fieldName] ) ? self::VALIDATORS[$fieldName] : null;

            if( ! empty( $validatorSpecs ) ) {
                foreach( $validatorSpecs as $validatorSpec ) {
                    if( ! isset( $validatorSpec['class'] ) ) {
                        throw new Exception( 'Invalid validator specs. Classname is missing.' );
                    }
                    $validatorClass = $validatorSpec['class'];
                    if( ! class_exists( $validatorClass ) ) {
                        throw new Exception( sprintf( 'Invalid validator specs. Class [%s] not found.', $validatorClass ) );
                    }

                    $validators[] = new $validatorClass( $validatorSpec );
                }

                $this->validatorInstances[$fieldName] = $validators;
            }

            return $validators;
        }

        return $this->validatorInstances[$fieldName];
    }

    /**
     * @param boolean $ignoreInvalidEntries
     *
     * @return $this
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws Exception
     */
    public function import( $ignoreInvalidEntries = false )
    {
        $file                             = $this->getSourceFile();
        $this->importSuccess              = false;
        $this->errors                     = $errors = array();
        $this->totalEntriesFound          = $totalEntriesFound = 0;
        $this->validEntries               = $validEntries = 0;
        $this->invalidEntries             = $invalidEntries = 0;
        $this->successFullImportedEntries = $successFullImportedEntries = 0;

        if( ! empty( $file ) && is_readable( $file ) ) {
            $entityManager    = $this->entityManager;
            $productClassMeta = $entityManager->getClassMetadata( Product::class );
            $dbConnection     = $entityManager->getConnection();

            $dbConnection->beginTransaction();

            $importFields = self::COLS;
            $reader       = new SpreadsheetReader();
            $phpExcel     = $reader->load( $file );
            $workSheet    = $phpExcel->getSheet( 0 );

            foreach( $workSheet->getRowIterator() as $row ) {
                // skip first row (title row)
                if( 1 >= $row->getRowIndex() ) {
                    continue;
                }

                $rowData   = array();
                $rowErrors = [];

                foreach( $importFields as $column => $name ) {
                    // $cell = $workSheet->getCellByColumnAndRow( $cellNumber ++, $row->getRowIndex() );
                    $cell       = $workSheet->getCell( "{$column}{$row->getRowIndex()}" );
                    $value      = $cell->getFormattedValue();
                    $validators = $this->getValidators( $name );

                    foreach( $validators as $validator ) {
                        if( $validator instanceof AbstractValidator ) {
                            if( ! $validator->isValid( $value ) ) {
                                $rowErrors[] = array(
                                    'col'                   => $column,
                                    'colName'               => $name,
                                    'errorMessage'          => sprintf(
                                        'Invaild value in row %s / col %s', $row->getRowIndex(), $column ),
                                    'validatorErrorMessage' => $validator->getErrorMessage()
                                );
                                continue;
                            }
                        }
                    }

                    $rowData[$name] = $value;
                }

                $totalEntriesFound ++;

                if( ! empty( $rowErrors ) ) {
                    $errors[] = array( 'rowIndex' => $row->getRowIndex(), 'errors' => $rowErrors );
                    $invalidEntries ++;
                    continue;
                }

                $validEntries ++;

                $product = new Product();

                foreach( $productClassMeta->getFieldNames() as $fieldName ) {
                    if( in_array( $fieldName, array( 'id', 'timeStamp' ) ) || ! isset( $rowData[$fieldName] ) ) {
                        continue;
                    }

                    $value = $rowData[$fieldName];

                    $product->{$fieldName} = $value;
                }

                $entityManager->persist( $product );
                $successFullImportedEntries ++;
            }

            $this->totalEntriesFound = $totalEntriesFound;
            $this->validEntries      = $validEntries;
            $this->invalidEntries    = $invalidEntries;

            if( empty( $errors ) || $ignoreInvalidEntries ) {
                $deleteQuery                   = $entityManager->createQuery( 'DELETE ' . Product::class );
                $alterTableResetAutoValueQuery = $entityManager->createNativeQuery(
                    "ALTER TABLE {$productClassMeta->getTableName()} AUTO_INCREMENT = 1", new ResultSetMapping() );

                $deleteQuery->execute();
                try {
                    $alterTableResetAutoValueQuery->execute();
                }
                catch( \PDOException $e ) { /* works anyway */
                }

                $entityManager->flush();

                $dbConnection->commit();

                $this->successFullImportedEntries = $successFullImportedEntries;
                $this->importSuccess              = true;
            } else if( ! empty( $errors ) ) {
                $this->errors = $errors;
                $dbConnection->rollBack();
            }

            return $this;
        }

        throw new Exception( 'No source file. Please upload source file first!' );
    }

    /**
     * @return bool
     */
    public function isImportSuccess()
    {
        return $this->importSuccess;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @return int
     */
    public function getSuccessFullImportedEntries()
    {
        return $this->successFullImportedEntries;
    }

    /**
     * @return int
     */
    public function getTotalEntriesFound()
    {
        return $this->totalEntriesFound;
    }

    /**
     * @return int
     */
    public function getValidEntries()
    {
        return $this->validEntries;
    }

    /**
     * @return int
     */
    public function getInvalidEntries()
    {
        return $this->invalidEntries;
    }
}
