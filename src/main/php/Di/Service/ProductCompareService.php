<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 17:34 - 05.02.18
 */

namespace netshake\SwissbitProductFinder\Di\Service;

use netshake\SwissbitProductFinder\Entity\Product;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Class ProductCompareService
 *
 * @package netshake\SwissbitProductFinder\Di\Service
 */
class ProductCompareService
{
    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var ProductService
     */
    private $productService;

    /**
     * @var Product[]
     */
    private $products = [];

    /**
     * UseCaseService constructor.
     *
     * @param SessionInterface $session
     * @param ProductService   $productService
     */
    public function __construct( SessionInterface $session, ProductService $productService )
    {
        $this->session        = $session;
        $this->productService = $productService;
    }

    /**
     * @return SessionInterface
     */
    public function getSession()
    {
        return $this->session;
    }

    /**
     * @return ProductService
     */
    public function getProductService()
    {
        return $this->productService;
    }

    /**
     * @return $this
     */
    public function restoreFromSession()
    {
        $this->setProducts( $this->getProductService()->findProductsFromIdList(
            (array) $this->getSession()->get( 'ProductCompareIds' ) ) );

        return $this;
    }

    /**
     * @return $this
     */
    public function rememberProducts()
    {
        $ids = [];
        foreach( $this->getProducts() as $product ) {
            $ids[] = $product->getId();
        }

        if( ! empty( $ids ) ) {
            $this->getSession()->set( 'ProductCompareIds', $ids );
        } else {
            $this->getSession()->remove( 'ProductCompareIds' );
        }

        return $this;
    }

    /**
     * @return array
     */
    public function getAllProductIds()
    {
        $ids = [];
        foreach( $this->getProducts() as $product ) {
            $ids[] = $product->getId();
        }

        return $ids;
    }

    /**
     * @return array
     */
    public function getAllProductManufacturerPartNumbers()
    {
        $mpn = [];
        foreach( $this->getProducts() as $product ) {
            $mpn[] = $product->getSwissbitPartNumber();
        }

        return $mpn;
    }

    /**
     * @return int
     */
    public function count()
    {
        return count( $this->products );
    }

    /**
     * @return Product[]
     */
    public function getProducts()
    {
        return $this->products ?: [];
    }

    /**
     * @param int|Product $product
     *
     * @return $this
     */
    public function addToCompare( $product )
    {
        if( ! $this->inList( $product ) ) {
            $this->products[] = $product instanceof Product
                ? $product : $this->getProductService()->find( intval( $product ) );
        }

        return $this;
    }

    /**
     * @param int|Product $product
     *
     * @return bool
     */
    public function inList( $product )
    {
        $productId = $product instanceof Product ? $product->getId() : intval( $product );
        foreach( $this->getProducts() as $product ) {
            if( $product->getId() === $productId ) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param int|Product $product
     *
     * @return $this
     */
    public function removeFromCompare( $product )
    {
        $productId   = $product instanceof Product ? $product->getId() : intval( $product );
        $productList = [];
        while( ! empty( $this->products ) ) {
            $product = array_shift( $this->products );
            if( $product->getId() == $productId ) {
                break;
            }
            $productList[] = $product;
        }

        $this->products = array_merge( $productList, $this->products );

        return $this;
    }

    /**
     * @param int[]|Product[] $productList
     *
     * @return $this
     */
    public function addProductsToCompare( array $productList )
    {
        foreach( $productList as $product ) {
            $this->addToCompare( $product );
        }

        return $this;
    }

    /**
     * @param Product[] $products
     *
     * @return $this
     */
    public function setProducts( array $products )
    {
        $this->clear();
        $this->addProductsToCompare( $products );

        return $this;
    }

    /**
     * @return $this
     */
    public function clear()
    {
        $this->products = [];

        return $this;
    }

    /**
     * @return array
     */
    public function createTableData()
    {
        $productCompareTableData = [];
        $products                = $this->getProducts();

        if( ! empty( $products ) ) {
            $body   = [];
            $images = [];

            $productCompareTableData['titles']  = $this->getAllProductManufacturerPartNumbers();
            $productCompareTableData['images']  =& $images;
            $productCompareTableData['body']    =& $body;
            $productCompareTableData['columns'] = $columns = count( $products ) + 1;

            foreach(
                [
                    [
                        'title'        => 'Image',
                        'propertyName' => 'productPictureWeblink',
                        'compare'      => false
                    ],
                    [
                        'title'        => 'Type',
                        'propertyName' => 'type',
                        'compare'      => true
                    ],
                    [
                        'title'        => 'Product Family',
                        'propertyName' => 'family',
                        'compare'      => true
                    ],
                    [
                        'title'        => 'Flash Type',
                        'propertyName' => 'flashType',
                        'compare'      => true
                    ],
                    [
                        'title'        => 'Density',
                        'propertyName' => 'density',
                        'compare'      => true
                    ],
                    [
                        'title'        => 'Endurance',
                        'propertyName' => 'endurance',
                        'compare'      => true
                    ],
                    [
                        'title'        => 'Rel Cost',
                        'propertyName' => 'relCost',
                        'compare'      => true
                    ],
                ] as $rowSpec
            ) {
                $propertyName  = $rowSpec['propertyName'];
                $cells         = [];
                $compareValues = [];
                $highlight     = false;
                $compare       = $rowSpec['compare'];

                foreach( $products as $product ) {
                    $value = $product->{$propertyName};

                    if( 'productPictureWeblink' == $propertyName ) {
                        $images[] = [ 'src' => $value ];
                        continue;
                    }

                    $cells[] = [ 'type' => 'text', 'value' => $value ];

                    if( !$compare ) {
                        continue;
                    }

                    // Not highlighted yet -> so check
                    if( ! $highlight && ! empty( $compareValues ) ) {
                        $highlight = ! in_array( $value, $compareValues );
                    }

                    $compareValues[] = $value;
                }

                if( ! empty( $cells ) ) {
                    $body[] = [
                        'title'     => $rowSpec['title'],
                        'highlight' => $highlight,
                        'cells'     => $cells,
                    ];
                }
            }
        }

        return $productCompareTableData;
    }
}
