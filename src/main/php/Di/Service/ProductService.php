<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 15:09 - 08.02.18
 */

namespace netshake\SwissbitProductFinder\Di\Service;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use netshake\SwissbitProductFinder\Entity\Product;

/**
 * Class ProductService
 *
 * @package netshake\SwissbitProductFinder\Di\Service
 */
class ProductService
{
    /**
     * @var array
     */
    const ATTRIBUTE_NAMES = [
        "status"                => "Type",
        "type"                  => "Product Type",
        "series"                => "Product Series",
        "family"                => "Product Family",
        "density"               => "Density",
        "densityClass"          => "Density Class",
        "swissbitPartNumber"    => "Swissbit Part Number",
        "description"           => "Description",
        "temperatureGrade"      => "Temp. Grade",
        "flashType"             => "Flash Type",
        "forNewDesign"          => "For new design",
        "factSheetWeblink"      => "Fact Sheet Weblink",
        "productPictureWeblink" => "Product Picture Weblink",
        "endurance"             => "Endurance",
        "relCost"               => "Rel Cost",
        "seqReadPerformance"    => "Seq. Read Performance MB/s",
        "seqWritePerformance"   => "Seq. Write Performance MB/s",
        "randReadPerformance"   => "Rnd. Read Performance IOPS",
        "randWritePerformance"  => "Rnd. Write Performance",
        "smallDataLogging"      => "Small data logging",
        "largeDataLogging"      => "Large Data logging",
        "audioVideoRecording"   => "Audio & Video Recording",
        "bootOnly"              => "Boot only",
        "bootVideoPlay"         => "Boot & Video Play",
        "bootExec"              => "Boot & Exec",
        "useCase7"              => "Eignung Use Case 7",
        "useCase8"              => "Eignung Use Case 8"
    ];

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var ProductFilterService
     */
    private $productFilterService;

    /**
     * ProductService constructor.
     *
     * @param EntityManager $entityManager
     */
    public function __construct( EntityManager $entityManager, ProductFilterService $productFilterService )
    {
        $this->entityManager        = $entityManager;
        $this->productFilterService = $productFilterService;
    }

    /**
     * @return EntityManager
     */
    public function getEntityManager()
    {
        return $this->entityManager;
    }

    /**
     * @return \Doctrine\ORM\EntityRepository
     */
    public function getRepository()
    {
        return $this->entityManager->getRepository( Product::class );
    }

    /**
     * @param int $id
     *
     * @return Product
     */
    public function find( $id )
    {
        return $this->entityManager->getRepository( Product::class )->find( $id );
    }

    /**
     * @param array $ids
     *
     * @return Product[]
     */
    public function findProductsFromIdList( array $ids )
    {
        if( ! empty( $ids ) ) {
            return $this->entityManager->getRepository( Product::class )->findBy( [ 'id' => $ids ] );
        }

        return [];
    }

    /**
     * @param int $manufacturerProductNumber
     *
     * @return Product[]
     */
    public function findOneByManufacturerProductNumber( $manufacturerProductNumber )
    {
        return $this->entityManager->getRepository( Product::class )->findBy( array( 'swissbitPartNumber' => $manufacturerProductNumber ) );
    }

    /**
     * @param $number
     *
     * @return Product[]
     */
    public function findRandomProducts( $number )
    {
        $classMeta   = $this->entityManager->getClassMetadata( Product::class );
        $rsm         = new ResultSetMappingBuilder( $this->entityManager );
        $nativeQuery = $this->entityManager->createNativeQuery(
            "SELECT id FROM {$classMeta->getTableName()} `p` ORDER BY RAND() LIMIT ?", $rsm );

        $rsm->addRootEntityFromClassMetadata( Product::class, 'p' );
        $nativeQuery->execute( [ $number ] );

        $ids = array_map( function ( $column ) {
            return current( $column );
        }, $nativeQuery->getArrayResult() );

        return $this->findproductsFromIdList( $ids );
    }

    /**
     * @param int $offset
     * @param int $limit
     *
     * @return Product[]
     */
    public function findAll( $offset = 0, $limit = null )
    {
        $qb = $this->entityManager->getRepository( Product::class )->createQueryBuilder( 'p' );

        if( $limit ) {
            $qb->setFirstResult( $offset )->setMaxResults( $limit );
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @param int $offset
     * @param int $limit
     *
     * @return Product[]
     */
    public function findAllUsingFilter( $offset = 0, $limit = null )
    {
        $qb = $this->entityManager->getRepository( Product::class )->createQueryBuilder( 'p' );

        $this->productFilterService->applyFilters( $qb, 'p' );

        if( $limit ) {
            $qb->setFirstResult( $offset )->setMaxResults( $limit );
        }

        $useCasePropertyName = $this->productFilterService->getUseCaseEntityPropertyName();
        if( !empty( $useCasePropertyName ) ) {
            $qb->orderBy( "p.{$useCasePropertyName}", "DESC" );
        }

//        $qb->orderBy( 'LENGTH(p.endurance)' );
//        $qb->orderBy( 'p.density', 'DESC' );

//        echo $qb->getQuery()->getSQL();
//        die();

        return $qb->getQuery()->getResult();
    }

    /**
     * @param array $ids
     *
     * @return array
     */
    public function findProductPartNumbers( $ids )
    {
        if( ! empty( $ids ) ) {
            $ids = (array) $ids;

            $qb = $this->entityManager->getRepository( Product::class )
                                      ->createQueryBuilder( 'p' )
                                      ->select( 'p.swissbitPartNumber' )
                                      ->where( 'p.id IN (' . implode( ',', $ids ) . ')' );

            return array_map( function ( $col ) {
                return reset( $col );
            }, (array) $qb->getQuery()->execute() );
        }

        return [];
    }

    /**
     * @return \Doctrine\ORM\Mapping\ClassMetadata
     */
    public function getClassMetadata()
    {
        return $this->entityManager->getClassMetadata( Product::class );
    }

    /**
     * @param $propertyName
     *
     * @return string
     */
    public function getProductPropertyDescription( $propertyName )
    {
        return self::ATTRIBUTE_NAMES[$propertyName] ?: null;
    }
}
