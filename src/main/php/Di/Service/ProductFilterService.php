<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 17:34 - 05.02.18
 */

namespace netshake\SwissbitProductFinder\Di\Service;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\QueryBuilder;
use netshake\SwissbitProductFinder\Exception;
use netshake\SwissbitProductFinder\ProductFilter\DensityFilter;
use netshake\SwissbitProductFinder\ProductFilter\EnduranceFilter;
use netshake\SwissbitProductFinder\ProductFilter\Filter\AbstractFilter;
use netshake\SwissbitProductFinder\ProductFilter\Filter\MultiOptionFilter;
use netshake\SwissbitProductFinder\ProductFilter\Filter\TextFilter;
use netshake\SwissbitProductFinder\ProductFilter\FlashTypeFilter;
use netshake\SwissbitProductFinder\ProductFilter\StatusFilter;
use netshake\SwissbitProductFinder\ProductFilter\TemperatureGradeFilter;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Class ProductFilterService
 *
 * @package netshake\SwissbitProductFinder\Di\Service
 */
class ProductFilterService
{
    /**
     * @var array
     */
    const USE_CASE_SPECS = [
        [
            'url_key'            => 'small-data-logging',
            'entityPropertyName' => 'smallDataLogging',
            'img'                => 'files/themes/swissbit/images/use_case_1.png',
            'title'              => 'Small data logging',
            'description'        => 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.'
        ],
        [
            'url_key'            => 'large-data-logging',
            'entityPropertyName' => 'largeDataLogging',
            'img'                => 'files/themes/swissbit/images/use_case_2.png',
            'title'              => 'Large data logging',
            'description'        => 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.'
        ],
        [
            'url_key'            => 'image-recording',
            'entityPropertyName' => 'imageRecording',
            'img'                => 'files/themes/swissbit/images/use_case_1.png',
            'title'              => 'Image recording',
            'description'        => 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.'
        ],
        [
            'url_key'            => 'video-recording',
            'entityPropertyName' => 'videoRecording',
            'img'                => 'files/themes/swissbit/images/use_case_2.png',
            'title'              => 'Video recording',
            'description'        => 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.'
        ],
        [
            'url_key'            => 'boot-and-program-execution',
            'entityPropertyName' => 'bootAndProgramExec',
            'img'                => 'files/themes/swissbit/images/use_case_1.png',
            'title'              => 'Boot & program execution',
            'description'        => 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.'
        ],
        [
            'url_key'            => 'boot-and-video-recording',
            'entityPropertyName' => 'bootAndVideoPlay',
            'img'                => 'files/themes/swissbit/images/use_case_2.png',
            'title'              => 'Boot & Video playback',
            'description'        => 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.'
        ],
        [
            'url_key'            => 'boot-and-database',
            'entityPropertyName' => 'bootAndDatabase',
            'img'                => 'files/themes/swissbit/images/use_case_1.png',
            'title'              => 'Boot & data base',
            'description'        => 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.'
        ],
        [
            'url_key'            => 'license-and-authentication',
            'entityPropertyName' => 'licenseAndAuthentication',
            'img'                => 'files/themes/swissbit/images/use_case_2.png',
            'title'              => 'Licence & authentication',
            'description'        => 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.'
        ]
    ];

    /**
     * @var GeneralService
     */
    private $generalService;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var string
     */
    private $filterId;

    /**
     * @return SessionInterface
     */
    public function getSession()
    {
        return $this->session;
    }

    /**
     * @return EntityManager
     */
    public function getEntiyManager()
    {
        return $this->entityManager;
    }

    /**
     * UseCaseService constructor.
     *
     * @param SessionInterface $session
     * @param EntityManager    $entityManager
     */
    public function __construct(
        GeneralService $generalService, SessionInterface $session, EntityManager $entityManager
    ) {
        $this->generalService = $generalService;
        $this->session        = $session;
        $this->entityManager  = $entityManager;
    }

    /**
     * @return string Path to directory
     *
     * @throws Exception
     */
    protected function getFilterStoreDirectory()
    {
        static $path;
        if( ! isset( $path ) ) {
            $path = $this->generalService->getApplicationHomeDirectory() . DIRECTORY_SEPARATOR . 'filters';
            if( ! is_dir( $path ) ) {
                if( ! mkdir( $path, 0775 ) ) {
                    $errorMessage = sprintf( 'Unable to create filte store directory (Path: %s).', $path );
                    unset( $directory );
                    throw new Exception( $errorMessage );
                }
            }
        }

        return $path;
    }

    /**
     * @return $this
     * @throws Exception
     */
    public function storeFilter()
    {
        if( ! isset( $this->filterId ) ) {
            $filterId = $this->filterId = uniqid();
        } else {
            $filterId = $this->filterId;
        }

        $file       = $this->getFilterStoreDirectory() . DIRECTORY_SEPARATOR . "$filterId.filter";
        $filterData = $this->getFilterData();

        if( ! file_put_contents( $file, json_encode( $filterData ) ) ) {
            throw new Exception( sprintf( "Failed to store filter. Unable to write file %s", $file ) );
        }

        $this->filterId = $filterId;
        $this->getSession()->set( 'ProductFilterId', $filterId );

        return $this;
    }

    /**
     * @param string $filterId
     *
     * @return $this
     * @throws Exception
     */
    public function restoreFilter( $filterId )
    {
        $file = $this->getFilterStoreDirectory() . DIRECTORY_SEPARATOR . "$filterId.filter";

        if( is_readable( $file ) ) {
            $data = json_decode( file_get_contents( $file ), true );
            $this->initFilters( $data );
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getFilterId()
    {
        return $this->filterId;
    }

    /**
     * @return $this
     */
    public function restoreFiltersFromSession()
    {
        $session = $this->getSession();
        $this->initFilters( $session->get( 'FilterData', array() ) );
        $this->filterId = $session->get( 'ProductFilterId' );

        return $this;
    }

    /**
     * @return $this
     */
    public function rememberFilterData()
    {
        $session = $this->getSession();
        $data    = $this->getFilterData();

        if( ! empty( $data ) ) {
            $session->set( 'FilterData', $data );
        } else {
            $session->remove( 'FilterData' );
        }

        return $this;
    }

    /**
     * @return array
     */
    public function getFilterData()
    {
        $data = array();
        foreach( $this->getFilters() as $filter ) {
            $data[$filter->getFilterName()] = $filter->getFilterData();
        }

        return $data;
    }


    /**
     * @param string $code
     *
     * @return $this
     */
    public function setUseCase( $code = null )
    {
        $filterData            = $this->getFilterData();
        $filterData['UseCase'] = $code;
        $this->initFilters( $filterData );

        return $this;
    }

    /**
     * @return string
     */
    public function getUseCase()
    {
        $filterData = $this->getFilterData();

        return isset( $filterData['UseCase'] ) ? $filterData['UseCase'] : null;
    }

    /**
     * @return array
     */
    public function getUseCaseInfo()
    {
        $useCaseKey  = $this->getUseCase();
        $useCaseInfo = array_filter( self::USE_CASE_SPECS, function ( $spec ) use ( $useCaseKey ) {
            return $spec['url_key'] == $useCaseKey;
        } );

        if( ! empty( $useCaseInfo ) ) {
            return reset( $useCaseInfo );
        }

        return [];
    }

    /**
     * @return string
     */
    public function getUseCaseEntityPropertyName()
    {
        $useCaseInfo = $this->getUseCaseInfo();

        if( ! empty( $useCaseInfo ) ) {
            return $useCaseInfo['entityPropertyName'];
        }

        return null;
    }

    /**
     * @param array $filterData
     *
     * @return $this
     */
    public function initFilters( array $filterData )
    {
        foreach( $this->getFilters() as $filter ) {
            if( isset( $filterData[$filter->getFilterName()] ) ) {
                $filter->setFilterData( $filterData[$filter->getFilterName()] );
            }
        }

        return $this;
    }

    /**
     * @return array
     */
    public function getActiveFilterNames()
    {
        $activeFilterNames = array();
        foreach( $this->getFilters() as $filter ) {
            if( ! $filter->isActive() ) {
                continue;
            }

            $activeFilterNames[] = $filter->getFilterName();
        }

        return $activeFilterNames;
    }

    /**
     * @return string
     */
    public function explain()
    {
        $text = "Der Benutzer hat folgender Filter-Einstellungen gemacht:\n\n";
        foreach( $this->getFilters() as $filter ) {
            $text .= $filter->toText() . PHP_EOL;
        }

        return $text;
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param string       $usingAlias
     *
     * @return $this
     */
    public function applyFilters( QueryBuilder $queryBuilder, $usingAlias )
    {
        $filters = $this->getFilters();

        if( count( $filters ) ) {
            $queryBuilder->where( '1 = 1' );
        }

        foreach( $filters as $filter ) {
            $filter->applyFilter( $queryBuilder, $usingAlias );
        }

        return $this;
    }

    /**
     * @return AbstractFilter[]
     */
    public function getFilters()
    {
        static $filters;
        if( ! isset( $filters ) ) {

            $useCaseMultioptions = [];
            array_map( function ( $useCasse ) use ( &$useCaseMultioptions ) {
                $useCaseMultioptions[$useCasse['url_key']] = $useCasse['title'];
            }, self::USE_CASE_SPECS );

            $filters = [
                new MultiOptionFilter( 'UseCase', [
                    'displayName'        => 'Use Case',
                    'disableUseForQuery' => true,
                    'multiOptions'       => $useCaseMultioptions
                ] ),

                new StatusFilter( [
                    'entityManager' => $this->getEntiyManager()
                ] ),

                new TextFilter( 'SwissbitPartNumber', [
                    'displayName' => 'Swissbit Part Number'
                ] ),

                new TemperatureGradeFilter( [
                    'entityManager' => $this->getEntiyManager()
                ] ),

                new FlashTypeFilter( [
                    'entityManager' => $this->getEntiyManager()
                ] ),

                new DensityFilter( [
                    'entityManager' => $this->getEntiyManager()
                ] ),

                new EnduranceFilter( [
                    'entityManager' => $this->getEntiyManager()
                ] )
            ];
        }

        return $filters;
    }
}
