<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 15:31 - 22.03.18
 */

namespace netshake\SwissbitProductFinder\Di\EventSubscriber;

use netshake\SwissbitProductFinder\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;

/**
 * Class KernelControllerSubscriber
 *
 * @package netshake\SwissbitProductFinder\Di\EventSubscriber
 */
class KernelControllerSubscriber
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * KernelControllerSubscriber constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct( ContainerInterface $container )
    {
        $this->container = $container;
    }

    /**
     * @param FilterControllerEvent $event
     */
    public function onKernelController( FilterControllerEvent $event )
    {
        $controllerList = $event->getController();
        if( ! is_array( $controllerList ) ) {
            return;
        }

        $controller = $controllerList[0];

        if( $controller instanceof AbstractController ) {
            $this->container->get( 'contao.framework' )->initialize();

            /** @var SessionInterface $session */
            $session = $this->container->get( 'session' );

            if( $session->has( 'SwissbitProductFinderLocale' ) ) {
                /** @var \Symfony\Component\Translation\DataCollectorTranslator $translator */
                $translator = \System::getContainer()->get( 'translator' );

                $request = $event->getRequest();

                $request->setLocale( $session->get( 'SwissbitProductFinderLocale' ) );
                $translator->setLocale( $request->getLocale() );
            }

            $controller->init();
        }
    }
}
