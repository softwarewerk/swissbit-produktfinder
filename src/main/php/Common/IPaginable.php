<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 12:50 - 13.02.18
 */

namespace netshake\SwissbitProductFinder\Common;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;

/**
 * Interface IPaginable
 *
 * @package netshake\SwissbitProductFinder
 */
interface IPaginable extends \Countable, \Traversable
{
    /**
     * @param int $number
     *
     * @return IPaginable
     */
    public function setPage( $number );

    /**
     * @return int
     */
    public function getNumberOfPages();
}
