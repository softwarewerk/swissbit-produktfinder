<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 14:36 - 09.02.18
 */

namespace netshake\SwissbitProductFinder\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="tl_swissbit_product")
 * @ORM\HasLifecycleCallbacks
 */
class Product
{
    /**
     * @var int
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var int
     * @ORM\Column(name="tstamp", type="integer", nullable=false)
     */
    protected $timeStamp;

    /**
     * @var string
     * @ORM\Column(name="SwissbitPartNumber", type="string", length=255, nullable=true)
     */
    public $swissbitPartNumber;

    /**
     * @var string
     * @ORM\Column(name="Description", type="string", length=255, nullable=false)
     */
    public $description;

    /**
     * @var string
     * @ORM\Column(name="Status", type="string", length=255, nullable=true)
     */
    public $status;

    /**
     * @var string
     * @ORM\Column(name="Type", type="string", length=255, nullable=true)
     */
    public $type;

    /**
     * @var string
     * @ORM\Column(name="Series", type="string", length=255, nullable=true)
     */
    public $series;

    /**
     * @var string
     * @ORM\Column(name="Family", type="string", length=255, nullable=true)
     */
    public $family;

    /**
     * @var string
     * @ORM\Column(name="Density", type="string", length=255, nullable=true)
     */
    public $density;

    /**
     * @var string
     * @ORM\Column(name="DensityClass", type="string", length=255, nullable=true)
     */
    public $densityClass;

    /**
     * @var string
     * @ORM\Column(name="TemperatureGrade", type="string", length=255, nullable=true)
     */
    public $temperatureGrade;

    /**
     * @var string
     * @ORM\Column(name="FlashType", type="string", length=255, nullable=true)
     */
    public $flashType;

    /**
     * @var string
     * @ORM\Column(name="ForNewDesign", type="string", length=10, nullable=true)
     */
    public $forNewDesign;

    /**
     * @var string
     * @ORM\Column(name="FactSheetWeblink", type="string", length=255, nullable=true)
     */
    public $factSheetWeblink;

    /**
     * @var string
     * @ORM\Column(name="ProductPictureWeblink", type="string", length=255, nullable=true)
     */
    public $productPictureWeblink;

    /**
     * @var string
     * @ORM\Column(name="Endurance", type="string", length=10, nullable=true)
     */
    public $endurance;

    /**
     * @var string
     * @ORM\Column(name="RelCost", type="string", length=10, nullable=true)
     */
    public $relCost;

    /**
     * @var string
     * @ORM\Column(name="SeqReadPerformance", type="string", length=10, nullable=true)
     */
    public $seqReadPerformance;

    /**
     * @var string
     * @ORM\Column(name="SeqWritePerformance", type="string", length=10, nullable=true)
     */
    public $seqWritePerformance;

    /**
     * @var string
     * @ORM\Column(name="RandReadPerformance", type="string", length=10, nullable=true)
     */
    public $randReadPerformance;

    /**
     * @var string
     * @ORM\Column(name="RandWritePerformance", type="string", length=10, nullable=true)
     */
    public $randWritePerformance;

    /**
     * Use Case
     *
     * @var int
     * @ORM\Column(name="SmallDataLogging", type="integer", nullable=true, options={"unsigned":true, "default": 0})
     */
    public $smallDataLogging;

    /**
     * Use Case
     *
     * @var string
     * @ORM\Column(name="LargeDataLogging", type="integer", nullable=true, options={"unsigned":true, "default": 0})
     */
    public $largeDataLogging;

    /**
     * Use Case
     *
     * @var string
     * @ORM\Column(name="ImageRecording", type="integer", nullable=true, options={"unsigned":true, "default": 0})
     */
    public $imageRecording;

    /**
     * Use Case
     *
     * @var string
     * @ORM\Column(name="VideoRecording", type="integer", nullable=true, options={"unsigned":true, "default": 0})
     */
    public $videoRecording;

    /**
     * Use Case
     *
     * @var string
     * @ORM\Column(name="BootAndProgramExec", type="integer", nullable=true, options={"unsigned":true, "default": 0})
     */
    public $bootAndProgramExec;

    /**
     * Use Case
     *
     * @var string
     * @ORM\Column(name="BootAndVideoPlay", type="integer", nullable=true, options={"unsigned":true, "default": 0})
     */
    public $bootAndVideoPlay;

    /**
     * Use Case
     *
     * @var string
     * @ORM\Column(name="BootAndDatabase", type="integer", nullable=true, options={"unsigned":true, "default": 0})
     */
    public $bootAndDatabase;

    /**
     * Use Case
     *
     * @var string
     * @ORM\Column(name="LicenseAndAuthentication", type="integer", nullable=true,
     *     options={"unsigned":true, "default": 0})
     */
    public $licenseAndAuthentication;

    /**
     * Product constructor.
     */
    public function __construct()
    {
        $this->timeStamp = time();
    }

//    /**
//     * @ORM\PostPersist
//     * @return void
//     */
//    public function onPostPersist()
//    {
//        if( empty( $this->timeStamp ) ) {
//            $this->timeStamp = time();
//        }
//    }
    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getSwissbitPartNumber()
    {
        return $this->swissbitPartNumber;
    }

    /**
     * @param string $swissbitPartNumber
     *
     * @return $this;
     */
    public function setSwissbitPartNumber( $swissbitPartNumber )
    {
        $this->swissbitPartNumber = $swissbitPartNumber;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription( $description )
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     *
     * @return Product
     */
    public function setStatus( $status )
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return Product
     */
    public function setType( $type )
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getSeries()
    {
        return $this->series;
    }

    /**
     * @param string $series
     *
     * @return Product
     */
    public function setSeries( $series )
    {
        $this->series = $series;

        return $this;
    }

    /**
     * @return string
     */
    public function getFamily()
    {
        return $this->family;
    }

    /**
     * @param string $family
     *
     * @return Product
     */
    public function setFamily( $family )
    {
        $this->family = $family;

        return $this;
    }

    /**
     * @return string
     */
    public function getDensity()
    {
        return $this->density;
    }

    /**
     * @param string $density
     *
     * @return Product
     */
    public function setDensity( $density )
    {
        $this->density = $density;

        return $this;
    }

    /**
     * @return string
     */
    public function getDensityClass()
    {
        return $this->densityClass;
    }

    /**
     * @param string $densityClass
     *
     * @return Product
     */
    public function setDensityClass( $densityClass )
    {
        $this->densityClass = $densityClass;

        return $this;
    }

    /**
     * @return string
     */
    public function getTemperatureGrade()
    {
        return $this->temperatureGrade;
    }

    /**
     * @param string $temperatureGrade
     *
     * @return Product
     */
    public function setTemperatureGrade( $temperatureGrade )
    {
        $this->temperatureGrade = $temperatureGrade;

        return $this;
    }

    /**
     * @return string
     */
    public function getFlashType()
    {
        return $this->flashType;
    }

    /**
     * @param string $flashType
     *
     * @return Product
     */
    public function setFlashType( $flashType )
    {
        $this->flashType = $flashType;

        return $this;
    }

    /**
     * @return string
     */
    public function getForNewDesign()
    {
        return $this->forNewDesign;
    }

    /**
     * @param string $forNewDesign
     *
     * @return Product
     */
    public function setForNewDesign( $forNewDesign )
    {
        $this->forNewDesign = $forNewDesign;

        return $this;
    }

    /**
     * @return string
     */
    public function getFactSheetWeblink()
    {
        return $this->factSheetWeblink;
    }

    /**
     * @param string $factSheetWeblink
     *
     * @return Product
     */
    public function setFactSheetWeblink( $factSheetWeblink )
    {
        $this->factSheetWeblink = $factSheetWeblink;

        return $this;
    }

    /**
     * @return string
     */
    public function getProductPictureWeblink()
    {
        return $this->productPictureWeblink;
    }

    /**
     * @param string $productPictureWeblink
     *
     * @return Product
     */
    public function setProductPictureWeblink( $productPictureWeblink )
    {
        $this->productPictureWeblink = $productPictureWeblink;

        return $this;
    }

    /**
     * @return string
     */
    public function getEndurance()
    {
        return $this->endurance;
    }

    /**
     * @param string $endurance
     *
     * @return Product
     */
    public function setEndurance( $endurance )
    {
        $this->endurance = $endurance;

        return $this;
    }

    /**
     * @return string
     */
    public function getRelCost()
    {
        return $this->relCost;
    }

    /**
     * @param string $relCost
     *
     * @return Product
     */
    public function setRelCost( $relCost )
    {
        $this->relCost = $relCost;

        return $this;
    }

    /**
     * @return string
     */
    public function getSeqReadPerformance()
    {
        return $this->seqReadPerformance;
    }

    /**
     * @param string $seqReadPerformance
     *
     * @return Product
     */
    public function setSeqReadPerformance( $seqReadPerformance )
    {
        $this->seqReadPerformance = $seqReadPerformance;

        return $this;
    }

    /**
     * @return string
     */
    public function getSeqWritePerformance()
    {
        return $this->seqWritePerformance;
    }

    /**
     * @param string $seqWritePerformance
     *
     * @return Product
     */
    public function setSeqWritePerformance( $seqWritePerformance )
    {
        $this->seqWritePerformance = $seqWritePerformance;

        return $this;
    }

    /**
     * @return string
     */
    public function getRandReadPerformance()
    {
        return $this->randReadPerformance;
    }

    /**
     * @param string $randReadPerformance
     *
     * @return Product
     */
    public function setRandReadPerformance( $randReadPerformance )
    {
        $this->randReadPerformance = $randReadPerformance;

        return $this;
    }

    /**
     * @return string
     */
    public function getRandWritePerformance()
    {
        return $this->randWritePerformance;
    }

    /**
     * @param string $randWritePerformance
     *
     * @return Product
     */
    public function setRandWritePerformance( $randWritePerformance )
    {
        $this->randWritePerformance = $randWritePerformance;

        return $this;
    }

    /**
     * @param string $useCasePropertyName
     *
     * @return int
     */
    public function getUseCaseValue( $useCasePropertyName )
    {
        if( !empty( $useCasePropertyName ) && isset( $this->{$useCasePropertyName} ) ) {
            return (int)$this->{$useCasePropertyName};
        }

        return 0;
    }
}
