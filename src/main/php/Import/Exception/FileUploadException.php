<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 17:11 - 22.03.18
 */

namespace netshake\SwissbitProductFinder\Import\Exception;

use netshake\SwissbitProductFinder\Import\Exception;

/**
 * Class FileUploadException
 *
 * @package netshake\SwissbitProductFinder\Import\Exception
 */
class FileUploadException extends Exception
{

}
