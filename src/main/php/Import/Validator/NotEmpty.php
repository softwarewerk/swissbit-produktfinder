<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 13:54 - 23.03.18
 */

namespace netshake\SwissbitProductFinder\Import\Validator;


class NotEmpty extends AbstractValidator
{
    /**
     * @param mixed $value
     *
     * @return boolean
     */
    public function isValid( $value )
    {
        return !empty( $value );
    }
}
