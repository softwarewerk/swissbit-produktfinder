<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 12:01 - 23.03.18
 */

namespace netshake\SwissbitProductFinder\Import\Validator;

/**
 * Class SwissbitPartNumberValidator
 *
 * @package netshake\SwissbitProductFinder\Import\Validator
 */
class SwissbitPartNumberValidator extends AbstractValidator
{
    /**
     * @param mixed $value
     *
     * @return boolean
     */
    public function isValid( $value )
    {
        if( ! empty( $value ) ) {
            return true;
        }

        $this->message = "The swissbit part number is required and may not be empty.";

        return false;
    }
}
