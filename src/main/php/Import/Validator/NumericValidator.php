<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 12:01 - 23.03.18
 */

namespace netshake\SwissbitProductFinder\Import\Validator;

/**
 * Class NumericValidator
 *
 * @package netshake\SwissbitProductFinder\Import\Validator
 */
class NumericValidator extends AbstractValidator
{
    /**
     * @var int
     */
    private $min;

    /**
     * @var int
     */
    private $max;

    /**
     * @param array $interval
     *
     * @return $this
     */
    public function setInterval( array $interval )
    {
        list( $min, $max ) = $interval;
        $this->min = intval( $min );
        $this->max = intval( $max );

        return $this;
    }

    /**
     * @param mixed $value
     *
     * @return boolean
     */
    public function isValid( $value )
    {
        if( is_numeric( $value ) ) {
            $value = (int) $value;
            if( isset( $this->min )
                && isset( $this->max )
                && $this->min > $value || $this->max < $value ) {
                $this->message = 'Value is not between min / max.';

                return false;
            } else if( isset( $this->min ) && $this->min > $value ) {
                $this->message = 'Value is lower between min.';

                return false;
            } else if( isset( $this->min ) && $this->min > $value ) {
                $this->message = 'Value is higher between max.';

                return false;
            }

            return true;
        }

        return false;
    }
}
