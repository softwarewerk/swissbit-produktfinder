<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 12:01 - 23.03.18
 */

namespace netshake\SwissbitProductFinder\Import\Validator;

/**
 * Class AbstractValidator
 *
 * @package netshake\SwissbitProductFinder\Import\Validator
 */
abstract class AbstractValidator
{
    /**
     * @var array
     */
    protected $options;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    protected $message;

    /**
     * @param mixed $value
     *
     * @return boolean
     */
    abstract public function isValid( $value );

    /**
     * AbstractValidator constructor.
     *
     * @param $options
     */
    public function __construct( $options )
    {
        $this->setOptions( $options );
    }

    /**
     * @param       $name
     * @param mixed $default
     *
     * @return mixed
     */
    public function get( $name, $default = null )
    {
        return $this->options[$name] ?: $default;
    }

    /**
     * @param array $options
     *
     * @return $this
     */
    public function setOptions( array $options = array() )
    {
        $this->options = array_filter( $options, function ( $val, $key ) {
            $method = "set" . ucfirst( $key );
            if( method_exists( $this, $method ) ) {
                $this->{$method}( $val );

                return true;
            }

            return false;
        }, ARRAY_FILTER_USE_BOTH );

        return $this;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName( $name )
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getErrorMessage()
    {
        return $this->message;
    }
}
