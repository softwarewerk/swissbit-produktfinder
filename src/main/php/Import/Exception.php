<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 17:10 - 22.03.18
 */

namespace netshake\SwissbitProductFinder\Import;

use netshake\SwissbitProductFinder\Exception as SwissbitProductFinderException;

/**
 * Class Exception
 *
 * @package netshake\SwissbitProductFinder\Import
 */
class Exception extends SwissbitProductFinderException {}
