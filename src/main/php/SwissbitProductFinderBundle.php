<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 14:41 - 30.01.18
 */

namespace netshake\SwissbitProductFinder;

# use netshake\InsivaCanteenBundle\DependencyInjection\InsivaCanteenExtension;
use netshake\SwissbitProductFinder\Di\SwissbitProductFinderExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class SwissbitProductFinderBundle extends Bundle
{
    /**
     * SwissbitProductFinderBundle constructor.
     */
    public function __construct()
    {
//       require_once 'vendor/autoload.php';
    }

    /**
     * Register extension
     *
     * @return SwissbitProductFinderExtension
     */
    public function getContainerExtension()
    {
        return new SwissbitProductFinderExtension();
    }
}
