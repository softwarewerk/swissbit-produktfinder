<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 15:28 - 21.03.18
 */

namespace netshake\SwissbitProductFinder\ProductFilter;

use Doctrine\ORM\EntityManager;
use netshake\SwissbitProductFinder\Entity\Product;
use netshake\SwissbitProductFinder\ProductFilter\Filter\MultiOptionFilter;

/**
 * Class DensityFilter
 *
 * @package netshake\SwissbitProductFinder\ProductFilter
 */
class DensityFilter extends MultiOptionFilter
{
    /**
     * DensityFilter constructor.
     *
     * @param array $options
     */
    public function __construct( array $options = array() )
    {
        parent::__construct( 'Density', array_merge( $options, array(
            'isMultiSelect' => true,
            'displayName'   => 'Density'
        ) ) );

        $entityManager = $this->getEntityManager();

        $qb = $entityManager->createQueryBuilder();
        $qb->select( 'DISTINCT p.density, TRIM( SUBSTRING(p.density, LOCATE(\' \', p.density))) as Unit' )
//           ->orderBy( 'Unit', 'DESC' )
//           ->addOrderBy( 'cast(p.density as unsigned)' )
           ->from( Product::class, 'p' );

        $multioptions = array_map( function ( $col ) {
            return $col['density'];
        }, $qb->getQuery()->getArrayResult() );

        $multioptions = array_combine( $multioptions, $multioptions );

        // TODO: Cache or at least solve via SQL
        uasort( $multioptions, function ( $a, $b ) {
            $a_unit = substr( $a, - 2 );
            $b_unit = substr( $b, - 2 );

            $unitCmp = strcmp( $b_unit, $a_unit );

            if( 0 == $unitCmp ) {
                return ( (int) $a <= (int) $b ) ? - 1 : 1;
            }

            return $unitCmp;
        } );

        $this->setMultiOptions( $multioptions );
    }
}
