<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 15:28 - 21.03.18
 */

namespace netshake\SwissbitProductFinder\ProductFilter;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\QueryBuilder;
use netshake\SwissbitProductFinder\Entity\Product;
use netshake\SwissbitProductFinder\ProductFilter\Filter\IntervalFilter;
use netshake\SwissbitProductFinder\ProductFilter\Filter\MultiOptionFilter;

/**
 * Class EnduranceFilter
 *
 * @package netshake\SwissbitProductFinder\ProductFilter
 */
class EnduranceFilter extends IntervalFilter
{
    /**
     * EnduranceFilter constructor.
     *
     * @param array $options
     */
    public function __construct( array $options = array() )
    {
        parent::__construct( 'Endurance', array_merge( $options, array(
            'displayName' => 'Endurance'
        ) ) );

        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select( 'DISTINCT LENGTH( p.endurance ) as endurance' )
           ->orderBy( 'endurance' )
           ->from( Product::class, 'p' );

//        $multioptions = array_map( function ( $col ) {
//            return $col['endurance'];
//        }, $qb->getQuery()->getArrayResult() );
//
//        $multioptions = array_combine( $multioptions, $multioptions );
//
//        $this->setRange( $multioptions );

        $results = array_map( function ( $col ) {
            return $col['endurance'];
        }, $qb->getQuery()->getArrayResult() );

        $this->setMin( min( $results ) );
        $this->setMax( max( $results ) );

        $this->setMinLabel( '*' );
        $this->setMaxLabel( '****' );
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param string       $usingAlias
     *
     * @return $this
     */
    public function applyFilter( QueryBuilder $queryBuilder, $usingAlias )
    {
        if( $this->isActive() && ! $this->isDisableUseForQuery() ) {
            $fieldName      = $this->getFieldName();
            $lowerValue     = intval( $this->getLowerValue() );
            $upperValue     = intval( $this->getUpperValue() );
            $paramNameLower = "{$fieldName}Lower";
            $paramNameUpper = "{$fieldName}Upper";

            if( $lowerValue == $upperValue ) {
                $queryBuilder->andWhere( "LENGTH({$usingAlias}.{$fieldName}) = :{$paramNameLower}" );
                $queryBuilder->setParameter( $paramNameLower, $upperValue, \PDO::PARAM_INT );
            } else {
                $queryBuilder->andWhere(
                    "LENGTH({$usingAlias}.{$fieldName}) BETWEEN :{$paramNameLower} AND :{$paramNameUpper}" );
                $queryBuilder->setParameter( $paramNameLower, $lowerValue, \PDO::PARAM_INT );
                $queryBuilder->setParameter( $paramNameUpper, $upperValue, \PDO::PARAM_INT );
            }
        }

        return $this;
    }
}
