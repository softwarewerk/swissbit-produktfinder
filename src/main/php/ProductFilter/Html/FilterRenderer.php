<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 18:34 - 20.03.18
 */

namespace netshake\SwissbitProductFinder\ProductFilter\Html;

use netshake\SwissbitProductFinder\ProductFilter\Html\FilterRenderer\AbstractFilterRenderer;
use netshake\SwissbitProductFinder\ProductFilter\Html\FilterRenderer\IntervalFilterRenderer;
use netshake\SwissbitProductFinder\ProductFilter\Html\FilterRenderer\MultiOptionFilterRenderer;
use netshake\SwissbitProductFinder\ProductFilter\Html\FilterRenderer\TextFilterRenderer;
use netshake\SwissbitProductFinder\ProductFilter\Filter\AbstractFilter;
use netshake\SwissbitProductFinder\ProductFilter\Filter\IntervalFilter;
use netshake\SwissbitProductFinder\ProductFilter\Filter\MultiOptionFilter;
use netshake\SwissbitProductFinder\ProductFilter\Filter\TextFilter;
use Symfony\Component\Translation\TranslatorInterface as Translator;

/**
 * Class FilterRenderer
 *
 * @package netshake\SwissbitProductFinder\ProductFilter\Html
 */
class FilterRenderer
{
    /**
     * @var AbstractFilter
     */
    private $filter;

    /**
     * @var Translator
     */
    private $translator;

    /**
     * FilterRenderer constructor.
     *
     * @param AbstractFilter $filter
     */
    public function __construct( AbstractFilter $filter = null )
    {
        $this->setFilter( $filter );
    }

    /**
     * @return Translator
     */
    public function getTranslator()
    {
        return $this->translator;
    }

    /**
     * @param Translator $translator
     */
    public function setTranslator( Translator $translator )
    {
        $this->translator = $translator;
    }

    /**
     * @return AbstractFilter
     */
    public function getFilter()
    {
        return $this->filter;
    }

    /**
     * @param AbstractFilter $filter
     *
     * @return $this
     */
    public function setFilter( AbstractFilter $filter = null )
    {
        $this->filter = $filter;

        return $this;
    }

    /**
     * @var AbstractFilterRenderer[]
     */
    private $renderer = [];

    /**
     * @param AbstractFilter $filter
     *
     * @return AbstractFilterRenderer
     * @throws \Exception
     */
    private function getRenderer( AbstractFilter $filter )
    {
        if( $filter instanceof TextFilter ) {
            $rendererClass = TextFilterRenderer::class;
        } else if( $filter instanceof MultiOptionFilter ) {
            $rendererClass = MultiOptionFilterRenderer::class;
        } else if( $filter instanceof IntervalFilter ) {
            $rendererClass = IntervalFilterRenderer::class;
        } else {
            throw new \Exception( 'Renderer not found.' );
        }

        if( ! isset( $this->renderer[$rendererClass] ) ) {
            $this->renderer[$rendererClass] = new $rendererClass( $this->getTranslator() );
        }

        return $this->renderer[$rendererClass]->setFilter( $filter );
    }


    /**
     * @return string HTML
     * @throws \Exception
     */
    public function render( $filter = null )
    {
        if( $filter instanceof AbstractFilter ) {
            $this->setFilter( $filter );
        }

        return $this->getRenderer( $this->getFilter() )->render();
    }
}
