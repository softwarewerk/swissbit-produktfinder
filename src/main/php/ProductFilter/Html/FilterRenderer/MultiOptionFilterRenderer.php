<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 14:02 - 20.03.18
 */

namespace netshake\SwissbitProductFinder\ProductFilter\Html\FilterRenderer;

use netshake\SwissbitProductFinder\ProductFilter\Filter\MultiOptionFilter;

/**
 * Class MultiOptionFilterRenderer
 *
 * @package netshake\SwissbitProductFinder\ProductFilter\Html\FilterRenderer
 */
class MultiOptionFilterRenderer extends AbstractFilterRenderer
{
    /**
     * @return string
     */
    protected final function renderFilter()
    {
        $filter = $this->getFilter();

        $html = "";
        if( $filter instanceof MultiOptionFilter ) {
            $options = $filter->getMultiOptions();

            $multiSelect = $filter->getIsMultiSelect();

            foreach( $options as $value => $label ) {
                $label = $this->translate( $label );

                $attributes = "";
                $attributes .= $filter->hasValue( $value ) ? " checked=\"checked\"" : "";

                if( $multiSelect ) {
                    $html .= <<<html
<div class="checkbox">
    <label>
        <input name="{$filter->getFilterName()}[$value]" type="hidden" value="0">
        <input {$attributes} name="{$filter->getFilterName()}[$value]" type="checkbox" value="1"> {$label}
    </label>
</div>
html;
                } else {
                    $html .= <<<html
<div class="radio">
    <label>
        <input {$attributes} name="{$filter->getFilterName()}" type="radio" value="{$value}"> {$label}
    </label>
</div>
html;
                }


            }
        }

        return $html;
    }
}
