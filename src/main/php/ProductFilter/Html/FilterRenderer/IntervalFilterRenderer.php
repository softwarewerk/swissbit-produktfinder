<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 14:02 - 20.03.18
 */

namespace netshake\SwissbitProductFinder\ProductFilter\Html\FilterRenderer;

use netshake\SwissbitProductFinder\ProductFilter\Filter\IntervalFilter;

/**
 * Class IntervalFilterRenderer
 *
 * @package netshake\SwissbitProductFinder\ProductFilter\Html\FilterRenderer
 */
class IntervalFilterRenderer extends AbstractFilterRenderer
{
    /**
     * @return string
     */
    protected final function renderFilter()
    {
        $filter = $this->getFilter();

        $html = "";
        if( $filter instanceof IntervalFilter ) {
            $range    = $filter->getRange();
            $lowerValue = $filter->getLowerValue();
            $upperValue = $filter->getUpperValue();

            if( empty( $range ) ) {
                $html = <<<html
<span style="display: inline-block; width: 49%; white-space: nowrap;">
    <label for="Lower">from</label> <input type="text" name="{$filter->getFilterName()}[lower]" value="{$lowerValue}" size="5" data-io-control-related="LowerHandle" />
</span>
<span style="display: inline-block; width: 49%; text-align: right; white-space: nowrap;">
    <label for="Upper">to</label> <input type="text" name="{$filter->getFilterName()}[upper]" value="{$upperValue}" size="5" data-io-control-related="UpperHandle" />
</span>
html;
            } else {
                $minOptionsHtml = "";
                $maxOptionsHtml = "";
                foreach( $range as $value => $label ) {
                    if( $value == $lowerValue ) {
                        $minOptionsHtml .= <<<html
<option selected="selected" value="{$value}">{$label}</option>
html;
                    } else {
                        $minOptionsHtml .= <<<html
<option value="{$value}">{$label}</option>
html;
                    }

                    if( $value == $upperValue ) {
                        $maxOptionsHtml .= <<<html
<option selected="selected" value="{$value}">{$label}</option>
html;
                    } else {
                        $maxOptionsHtml .= <<<html
<option value="{$value}">{$label}</option>
html;
                    }
                }

                $html = <<<html
<span style="display: inline-block; width: 49%; white-space: nowrap;">
    <label for="Lower">from</label> <select name="{$filter->getFilterName()}[lower]" data-io-control-related="LowerHandle">{$minOptionsHtml}</select>
</span>
<span style="display: inline-block; width: 49%; text-align: right; white-space: nowrap;">
    <label for="Upper">to</label> <select name="{$filter->getFilterName()}[upper]" data-io-control-related="UpperHandle">{$maxOptionsHtml}</select> MB/s
</span>
html;
            }

            $html = <<<html
<div class="row">
    <div class="col-xs-12">
        <div data-io-control="IntervalSlider" data-min="{$filter->getMin()}" data-max="{$filter->getMax()}"><!-- class="io-control-slider-replace" -->
        {$html}
        </div>
    </div>
</div>
html;
        }

        return $html;
    }
}
