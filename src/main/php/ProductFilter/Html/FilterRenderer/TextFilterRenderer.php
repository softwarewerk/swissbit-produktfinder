<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 14:02 - 20.03.18
 */

namespace netshake\SwissbitProductFinder\ProductFilter\Html\FilterRenderer;

use netshake\SwissbitProductFinder\ProductFilter\Filter\TextFilter;

/**
 * Class TextFilterRenderer
 *
 * @package netshake\SwissbitProductFinder\ProductFilter\Html\FilterRenderer
 */
class TextFilterRenderer extends AbstractFilterRenderer
{
    /**
     * @return string
     */
    protected final function renderFilter()
    {
        $filter = $this->getFilter();

        if( $filter instanceof TextFilter ) {
            return <<<html
<div class="form-group-sm">
    <input  class="form-control"
            placeholder="{$filter->getDisplayName()}"
            type="text"
            name="{$this->getFilter()->getFilterName()}"
            value="{$filter->getQuery()}"/>
</div>
html;
        }

        return "";
    }
}
