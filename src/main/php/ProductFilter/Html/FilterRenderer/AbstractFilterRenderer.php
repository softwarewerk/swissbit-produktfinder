<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 14:13 - 20.03.18
 */

namespace netshake\SwissbitProductFinder\ProductFilter\Html\FilterRenderer;

use netshake\SwissbitProductFinder\ProductFilter\Filter\AbstractFilter;
use Symfony\Component\Translation\TranslatorInterface as Translator;

/**
 * Class AbstractFilterRenderer
 *
 * @package netshake\SwissbitProductFinder\ProductFilter\Html\FilterRenderer
 */
abstract class AbstractFilterRenderer
{
    /**
     * @var AbstractFilter
     */
    private $filter;

    /**
     * @var Translator
     */
    private $translator;

    /**
     * @param Translator $translator
     */
    public function setTranslator( Translator $translator )
    {
        $this->translator = $translator;
    }

    /**
     * @return Translator
     */
    public function getTranslator()
    {
        return $this->translator;
    }

    /**
     * @param string $key
     *
     * @return string
     */
    protected function translate( $key )
    {
        if( $this->translator ) {
            return $this->translator->trans( $key );
        }

        return $key;
    }

    /**
     * @return string
     */
    public function render()
    {
        $filter      = $this->getFilter();
        $displayName = $filter->getDisplayName() ?: $filter->getFilterName();
        $isActive = $filter->isActive();

        $additionalPanelClass = $isActive ? "panel-primary" : "";

        return <<<html
<div class="panel panel-default {$additionalPanelClass} panel-condensed filter" data-filter="{$filter->getFilterName()}">
    <div    class="panel-heading"
            id="Heading{$filter->getFilterName()}"
            role="tab"
            data-toggle="collapse"
            data-parent="[role=tablist]"
            data-target="#Collapse{$filter->getFilterName()}"
            aria-expanded="false"
            aria-controls="Collapse{$filter->getFilterName()}">
        <h4 class="panel-title">{$this->translate( $displayName )}</h4>
    </div>
    <div class="panel-dropdown panel-sm-dropdown">
        <div    id="Collapse{$filter->getFilterName()}"
                class="panel-collapse collapse"
                role="tabpanel"
                aria-labelledby="Heading{$filter->getFilterName()}">
            <div class="panel-body">

                <div class="row">
                    <div class="col-xs-12">
                        {$this->renderFilter()}
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
html;
    }

    /**
     * @return string
     */
    protected function renderFilter()
    {
        return '';
    }

    /**
     * AbstractFilterRenderer constructor.
     *
     * @param Translator $translator
     */
    public function __construct( Translator $translator )
    {
        $this->setTranslator( $translator );
    }

    /**
     * @return AbstractFilter
     */
    public function getFilter()
    {
        return $this->filter;
    }

    /**
     * @param AbstractFilter $filter
     *
     * @return $this
     */
    public function setFilter( $filter )
    {
        $this->filter = $filter;

        return $this;
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this->render();
    }
}
