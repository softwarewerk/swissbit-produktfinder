<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 17:46 - 20.03.18
 */

namespace netshake\SwissbitProductFinder\ProductFilter\Filter;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\QueryBuilder;

/**
 * Class AbstractFilter
 *
 * @package netshake\SwissbitProductFinder\ProductFilter\Filter
 */
abstract class AbstractFilter
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var string
     */
    protected $filterName;

    /**
     * @var mixed
     */
    protected $filterData;

    /**
     * @var string
     */
    protected $displayName;

    /**
     * @var array
     */
    protected $options;

    /**
     * @var string
     */
    protected $fieldName;

    /**
     * @var bool
     */
    protected $disableUseForQuery = false;

    /**
     * AbstractFilter constructor.
     *
     * @param string $filterName
     * @param array  $options
     */
    public function __construct( $filterName, array $options = array() )
    {
        $this->filterName = $filterName;

        $this->setOptions( $options );
    }

    /**
     * @return string
     */
    abstract public function toText();

    /**
     * @param QueryBuilder $queryBuilder
     * @param string       $usingAlias
     *
     * @return $this
     */
    abstract public function applyFilter( QueryBuilder $queryBuilder, $usingAlias );

    /**
     * @param bool $flag
     *
     * @return $this
     */
    public function setDisableUseForQuery( $flag = true )
    {
        $this->disableUseForQuery = ( true === $flag );

        return $this;
    }

    /**
     * @return bool
     */
    public function isDisableUseForQuery()
    {
        return true === $this->disableUseForQuery;
    }

    /**
     * @param string $fieldName
     *
     * @return $this
     */
    public function setFiledName( $fieldName )
    {
        $this->fieldName = $fieldName;

        return $this;
    }

    /**
     * @return string
     */
    public function getFieldName()
    {
        if( ! isset( $this->fieldName ) ) {
            return lcfirst( $this->getFilterName() );
        }

        return $this->fieldName;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return ! empty( $this->filterData );
    }

    /**
     * @return EntityManager
     */
    public function getEntityManager()
    {
        return $this->entityManager;
    }

    /**
     * @param EntityManager $entityManager
     *
     * @return $this
     */
    public function setEntityManager( $entityManager )
    {
        $this->entityManager = $entityManager;

        return $this;
    }

    /**
     * @param mixed $data
     *
     * @return $this
     */
    public function setFilterData( $data )
    {
        $this->filterData = $data;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFilterData()
    {
        return $this->filterData;
    }

    /**
     * @param array $options
     *
     * @return $this
     */
    public function setOptions( array $options = array() )
    {
        $this->options = array_filter( $options, function ( $val, $key ) {
            $method = "set" . ucfirst( $key );
            if( method_exists( $this, $method ) ) {
                $this->{$method}( $val );

                return true;
            }

            return false;
        }, ARRAY_FILTER_USE_BOTH );

        return $this;
    }

    /**
     * @return string
     */
    public function getFilterName()
    {
        return $this->filterName;
    }

    /**
     * @param string $filterName
     *
     * @return $this
     */
    public function setFilterName( $filterName )
    {
        $this->filterName = $filterName;

        return $this;
    }

    /**
     * @param string $displayName
     *
     * @return $this
     */
    public function setDisplayName( $displayName )
    {
        $this->displayName = $displayName;

        return $this;
    }

    /**
     * @return string
     */
    public function getDisplayName()
    {
        return $this->displayName;
    }
}
