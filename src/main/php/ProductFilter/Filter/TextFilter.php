<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 17:45 - 20.03.18
 */

namespace netshake\SwissbitProductFinder\ProductFilter\Filter;

use Doctrine\ORM\QueryBuilder;

/**
 * Class TextFilter
 *
 * @package netshake\SwissbitProductFinder\ProductFilter\Filter
 */
class TextFilter extends AbstractFilter
{
    /**
     * @param QueryBuilder $queryBuilder
     * @param string       $usingAlias
     *
     * @return $this
     */
    public function applyFilter( QueryBuilder $queryBuilder, $usingAlias )
    {
        if( $this->isActive() && !$this->isDisableUseForQuery() ) {
            $queryBuilder->andWhere( "{$usingAlias}.swissbitPartNumber LIKE :{$this->getFieldName()}" );
            $queryBuilder->setParameter( $this->getFieldName(), "%{$this->getQuery()}%", \PDO::PARAM_STR );
        }

        return $this;
    }

    /**
     * @return string
     */
    public function toText()
    {
        if( $this->isActive() ) {
            return "{$this->getDisplayName()}: {$this->getQuery()}";
        }

        return "{$this->getDisplayName()}: Keine Eingabe";
    }

    /**
     * @param string $query
     *
     * @return $this
     */
    public function setQuery( $query )
    {
        $this->setFilterData( $query );

        return $this;
    }

    /**
     * @return string
     */
    public function getQuery()
    {
        return $this->getFilterData();
    }
}
