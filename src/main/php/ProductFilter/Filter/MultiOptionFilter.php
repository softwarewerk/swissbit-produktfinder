<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 17:45 - 20.03.18
 */

namespace netshake\SwissbitProductFinder\ProductFilter\Filter;

use Doctrine\ORM\QueryBuilder;

/**
 * Class MultiOptionFilter
 *
 * @package netshake\SwissbitProductFinder\ProductFilter\Filter
 */
class MultiOptionFilter extends AbstractFilter
{
    /**
     * @var array
     */
    private $multiOptions;

    /**
     * @var bool
     */
    private $isMultiSelect = false;

    /**
     * @return array
     */
    public function getSelectedValues()
    {
        if( $this->getIsMultiSelect() ) {
            return array_keys( array_filter( $this->getFilterData(), function ( $selected ) {
                return (bool) $selected;
            } ) );
        }

        return [ $this->getFilterData() ];
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param string       $usingAlias
     *
     * @return $this
     */
    public function applyFilter( QueryBuilder $queryBuilder, $usingAlias )
    {
        if( $this->isActive() && ! $this->isDisableUseForQuery() ) {
            $fieldName      = $this->getFieldName();
            $selectedValues = $this->getSelectedValues();
            if( ! empty( $selectedValues ) ) {
                $queryBuilder->andWhere( "{$usingAlias}.{$fieldName} IN ('" . implode( "', '", $selectedValues ) . "')" );
            }
        }

        return $this;
    }

    /**
     * @return string
     */
    public function toText()
    {
        if( $this->isActive() ) {
            if( $this->getIsMultiSelect() ) {
                $values = array_keys( array_filter( (array) $this->filterData, function ( $val ) {
                    return (bool) $val;
                } ) );

                return "{$this->getDisplayName()}: " . "[" . implode( ', ', $values ) . "]";
            }

            $label = $this->multiOptions[$this->filterData];

            return "{$this->getDisplayName()}: {$label}";
        }

        return "{$this->getDisplayName()}: Keine Auswahl";
    }

    /**
     * @param bool $flag
     *
     * @return $this
     */
    public function setIsMultiSelect( $flag = true )
    {
        $this->isMultiSelect = (bool) $flag;

        return $this;
    }

    /**
     * @return bool
     */
    public function getIsMultiSelect()
    {
        return (bool) $this->isMultiSelect;
    }


    /**
     * @return array
     */
    public function getMultiOptions()
    {
        return $this->multiOptions;
    }

    /**
     * @param array $options
     *
     * @return $this
     */
    public function setMultiOptions( array $options )
    {
        $this->multiOptions = $options;

        return $this;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        if( $this->getIsMultiSelect() ) {
            if( is_array( $this->filterData ) ) {
                $values = array_keys( array_filter( $this->filterData, function ( $val ) {
                    return (bool) $val;
                } ) );

                return ! empty( $values );
            }

            return false;
        }

        return ! empty( $this->filterData );
    }

    /**
     * @param mixed $value
     *
     * @return bool
     */
    public function hasValue( $value )
    {
        if( $this->getIsMultiSelect() ) {
            if( is_array( $this->filterData ) ) {
                return in_array( $value, array_keys( array_filter( $this->filterData, function ( $val ) {
                    return (bool) $val;
                } ) ) );
            }

            return false;
        }

        return $this->filterData == $value;
    }
}
