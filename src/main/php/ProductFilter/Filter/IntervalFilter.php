<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 17:45 - 20.03.18
 */

namespace netshake\SwissbitProductFinder\ProductFilter\Filter;

use Doctrine\ORM\QueryBuilder;

/**
 * Class IntervalFilter
 *
 *
//                new IntervalFilter( 'IntervalFilter', [
//                    'displayName' => 'Interval Filter',
//                    'min'         => 20,
//                    'max'         => 100,
////                    'range'       => array(
////                        16   => '16',
////                        32   => '32',
////                        64   => '64',
////                        128  => '128',
////                        256  => '256',
////                        512  => '512',
////                        1024 => '1024',
////                        2048 => '2048',
////                        4096 => '4096',
////                    )
//                ] )
 *
 * @package netshake\SwissbitProductFinder\ProductFilter\Filter
 */
class IntervalFilter extends AbstractFilter
{
    /**
     * @var array
     */
    private $range;

    /**
     * @var mixed
     */
    private $min = 0;

    /**
     * @var mixed
     */
    private $max = 0;

    /**
     * @var string
     */
    private $unit;

    /**
     * @var string
     */
    private $minLabel;

    /**
     * @var string
     */
    private $maxLabel;

    /**
     * @param QueryBuilder $queryBuilder
     * @param string       $usingAlias
     *
     * @return $this
     */
    public function applyFilter( QueryBuilder $queryBuilder, $usingAlias )
    {
        return $this;
    }

    /**
     * @param string $unit
     *
     * @return $this
     */
    public function setUnit( $unit )
    {
        $this->unit = $unit;

        return $this;
    }

    /**
     * @return string
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * @param string $minLabel
     *
     * @return $this
     */
    public function setMinLabel( $minLabel )
    {
        $this->minLabel = $minLabel;

        return $this;
    }

    /**
     * @return string
     */
    public function getMinLabel()
    {
        return $this->minLabel;
    }


    /**
     * @param string $maxLabel
     *
     * @return $this
     */
    public function setMaxLabel( $maxLabel )
    {
        $this->maxLabel = $maxLabel;

        return $this;
    }

    /**
     * @return string
     */
    public function getMaxLabel()
    {
        return $this->maxLabel;
    }

    /**
     * @return string
     */
    public function toText()
    {
        if( $this->isActive() ) {
            return "{$this->getDisplayName()}: [Min: {$this->getLowerValue()}, Max: {$this->getUpperValue()}]";
        }

        return "{$this->getDisplayName()}: Keine Auswahl";
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return ( ( $this->getMin() != $this->getLowerValue() )
                 || ( $this->getMax() != $this->getUpperValue() ) );
    }

    /**
     * @return mixed
     */
    public function getLowerValue()
    {
        if( is_array( $this->filterData ) && ! empty( $this->filterData['lower'] ) ) {
            return $this->filterData['lower'];
        }

        return $this->getMin();
    }

    /**
     * @return mixed
     */
    public function getUpperValue()
    {
        if( is_array( $this->filterData ) && ! empty( $this->filterData['upper'] ) ) {
            return $this->filterData['upper'];
        }

        return $this->getMax();
    }

    /**
     * @return mixed
     */
    public function getMin()
    {
        $range = $this->getRange();
        if( ! empty( $range ) ) {
            return array_shift( $range );
        }

        return $this->min;
    }

    /**
     * @param mixed $value
     *
     * @return $this
     */
    public function setMin( $value )
    {
        $this->min = $value;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getMax()
    {
        $range = $this->getRange();
        if( ! empty( $range ) ) {
            return array_pop( $range );
        }

        return $this->max;
    }

    /**
     * @param mixed $max
     *
     * @return $this
     */
    public function setMax( $max )
    {
        $this->max = $max;

        return $this;
    }

    /**
     * @param array $range
     *
     * @return $this
     */
    public function setRange( array $range )
    {
        $this->range = $range;

        return $this;
    }

    /**
     * @return array
     */
    public function getRange()
    {
        return $this->range;
    }
}
