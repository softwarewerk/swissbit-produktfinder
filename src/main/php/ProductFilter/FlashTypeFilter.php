<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 15:28 - 21.03.18
 */

namespace netshake\SwissbitProductFinder\ProductFilter;

use Doctrine\ORM\EntityManager;
use netshake\SwissbitProductFinder\Entity\Product;
use netshake\SwissbitProductFinder\ProductFilter\Filter\MultiOptionFilter;

/**
 * Class FlashTypeFilter
 *
 * @package netshake\SwissbitProductFinder\ProductFilter
 */
class FlashTypeFilter extends MultiOptionFilter
{
    /**
     * FlashTypeFilter constructor.
     *
     * @param array $options
     */
    public function __construct( array $options = array() )
    {
        parent::__construct( 'FlashType', array_merge( $options, array(
            'isMultiSelect' => true,
            'displayName' => 'Flash Type'
        ) ) );

        $entityManager = $this->getEntityManager();

        $qb = $entityManager->createQueryBuilder();
        $qb->select( 'DISTINCT p.flashType' )
           ->addOrderBy( 'p.flashType' )
           ->from( Product::class, 'p' );

        $multioptions = array_map( function ( $col ) {
            return $col['flashType'];
        }, $qb->getQuery()->getArrayResult() );

        $multioptions = array_combine( $multioptions, $multioptions );

        $this->setMultiOptions( $multioptions );
    }
}
