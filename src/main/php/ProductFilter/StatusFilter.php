<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 15:28 - 21.03.18
 */

namespace netshake\SwissbitProductFinder\ProductFilter;

use Doctrine\ORM\EntityManager;
use netshake\SwissbitProductFinder\Entity\Product;
use netshake\SwissbitProductFinder\ProductFilter\Filter\MultiOptionFilter;

/**
 * Class StatusFilter
 *
 * @package netshake\SwissbitProductFinder\ProductFilter
 */
class StatusFilter extends MultiOptionFilter
{
    /**
     * StatusFilter constructor.
     *
     * @param array $options
     */
    public function __construct( array $options = array() )
    {
        parent::__construct( 'Status', array_merge( $options, array(
            'isMultiSelect' => true,
            'displayName' => 'Status'
        ) ) );

        $entityManager = $this->getEntityManager();

        $qb = $entityManager->createQueryBuilder();
        $qb->select( 'DISTINCT p.status' )
           ->addOrderBy( 'p.status' )
           ->from( Product::class, 'p' );

        $multioptions = array_map( function ( $col ) {
            return $col['status'];
        }, $qb->getQuery()->getArrayResult() );

        $multioptions = array_combine( $multioptions, $multioptions );

        $this->setMultiOptions( $multioptions );
    }
}
