<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 15:28 - 21.03.18
 */

namespace netshake\SwissbitProductFinder\ProductFilter;

use Doctrine\ORM\EntityManager;
use netshake\SwissbitProductFinder\Entity\Product;
use netshake\SwissbitProductFinder\ProductFilter\Filter\MultiOptionFilter;

/**
 * Class TemperatureGradeFilter
 *
 * @package netshake\SwissbitProductFinder\ProductFilter
 */
class TemperatureGradeFilter extends MultiOptionFilter
{
    /**
     * TemperatureGradeFilter constructor.
     *
     * @param array $options
     */
    public function __construct( array $options = array() )
    {
        parent::__construct( 'TemperatureGrade', array_merge( $options, array(
            'isMultiSelect' => true,
            'displayName'   => 'Temperature Grade'
        ) ) );

        $entityManager = $this->getEntityManager();

        $qb = $entityManager->createQueryBuilder();
        $qb->select( 'DISTINCT p.temperatureGrade' )
           ->addOrderBy( 'p.temperatureGrade' )
           ->from( Product::class, 'p' );

        $multioptions = array_map( function ( $col ) {
            return $col['temperatureGrade'];
        }, $qb->getQuery()->getArrayResult() );

        $multioptions = array_combine( $multioptions, $multioptions );

        $this->setMultiOptions( $multioptions );
    }
}
