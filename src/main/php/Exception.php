<?php
/**
 * @copyright netshake GmbH <labs@netshake.de>
 * @author    chris <chris@netshake.de>
 * Creationtime: 13:22 - 29.03.18
 */

namespace netshake\SwissbitProductFinder;

/**
 * Class Exception
 *
 * @package netshake\SwissbitProductFinder
 */
class Exception extends \Exception
{

}
